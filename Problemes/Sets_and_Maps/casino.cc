#include <iostream>
#include <map>
#include <string>

using namespace std;

int main() {
	map<string, int> casino;
	
	string name, action;
	
	while (cin >> name >> action) {
		auto it = casino.find(name);
		if (action == "enters") {
			if (it != casino.end()) cout << name << " is already in the casino" << endl;
			else casino.insert(pair<string, int>(name, 0)); // init player with 0 money
		}
		else if (action == "leaves") {
			if (it == casino.end()) cout << name << " is not in the casino" << endl;
			else {
				cout << name << " has won " << (*it).second << endl;
				casino.erase(it);
			}
		}
		else if (action == "wins") {
			int q;
			cin >> q;
			if (it == casino.end()) cout << name << " is not in the casino" << endl;
			else it -> second += q;
		}			
	}
	
	cout << "----------" << endl;
	
	for (auto it = casino.begin(); it != casino.end(); ++it) 
		cout <<  it -> first << " is winning " << it -> second << endl;
	
	return 0;
}
		
