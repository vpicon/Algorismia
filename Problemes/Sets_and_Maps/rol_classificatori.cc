#include <iostream>
#include <string>
#include <map>
#include <set>


using namespace std;
using P = pair<string, int>;

int main() {
	map<string, int> Melo, Mlog;  // map of elo and map of logged players (1/0):(on/off)
	string instruction;
	while (cin >> instruction) {
		if (instruction == "LOGIN") {
			string player;
			cin >> player;
			auto it = Mlog.find(player);
			if (it == Mlog.end()) {  // player hasnt previously logged, then add it 
				Mlog.insert(P(player, 1));
				Melo.insert(P(player, 1200));  
			}
			else it -> second = 1;  // no problem if already logged in		
		}
		else if (instruction == "LOGOUT") {
			string player;
			cin >> player;
			auto it = Mlog.find(player);
			if (it != Mlog.end()) it -> second = 0;  // only do something if player already logged before
		}
		else if (instruction == "GET_ELO") {
			string player;
			cin >> player;
			auto it = Melo.find(player);
			cout << player << ' ' << it -> second << endl;
			
			
		}
		else {  // PLAY instruction
			string player1, player2;
			cin >> player1 >> player2;
			auto it1 = Melo.find(player1);
			auto it2 = Melo.find(player2);
			if (it1 != Melo.end() and it2 != Melo.end() and
				Mlog[player1] == 1 and Mlog[player2] == 1 ) {
				it1 -> second += 10;
				it2 -> second = max(1200, it2->second);
			}
			else cout << "jugador(s) no connectat(s)" << endl;
		}
	}
	
	set< pair<int,string>> ordered;
	for (auto p : Melo) 
		ordered.insert( pair<int, string> (-p.second, p.first) );
	cout << endl;
	cout << "RANKING" << endl;
	for (auto p : ordered) 
		cout << p.second << ' ' << -p.first << endl;
}
