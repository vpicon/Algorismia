#include <iostream>
#include <vector>
#include <string>

using namespace std;
using VI = vector<int>;

int N = 100; // v size bound
VI v; // v[i] gives the number of increasing subsequences that finish in i with at least two characters
string s;

// computes v[i];
int f(int i) { 
	int& res = v[i];
	if (res !=  -1) return res;
	res = 0;
	if (i == 0) return 0;
	for (int j = 0; j < i; j++) {
		if (s[j] < s[i]) res+= v[j]+1;
	}
	return res;
}






int main() {
	while (cin >> s) {
		v = VI(N, -1); // revisar
		if ((int)s.size() < 2) cout << 0 << endl;
		else {
			int sum = 0;
			for (int i = 0; i < (int)s.size(); i++) sum += f(i); // revisar imax
			cout << sum << endl;
		}
	}
	return 0;
}
