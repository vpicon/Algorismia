#include <iostream>
#include <vector>

using namespace std;
using ll = long long;
using VI = vector<ll>;

int n;
VI S, D;

ll d(int n) {
	if (n < 0) return 0;
	ll& res = D[n];
	if (res != -1) return res;
	if (n == 0 or n == 1) return res = 1;
	if (n == 2) return res = 2;
	else return res = d(n - 2) + d(n - 3);
}


ll s(int n) {
	ll& res = S[n];
	if(res != -1) return res;
	if (n == 0) res = 1;
	return res = d(n) + d(n - 1);
}
	

int main() {
	S = VI(151, -1);
	D = VI(151, -1);
	while (cin >> n) cout << s(n) << endl;
	return 0;
}
	
