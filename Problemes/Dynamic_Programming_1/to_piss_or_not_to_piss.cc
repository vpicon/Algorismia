#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;



int n;  // Number of available sites.
float p;  // Precission
VI v;  // v[n] diu quants llocs s'omplen, de n disponibles. 
VVI W; // W[n, x] diu minim # parets calen per encabir x persones en n llocs


// Calcula quants llocs x s'omplen segons l'algorisme de n disponibles
int x_algo(int n) {
	if (n <= 0 or n > 500) return 0;
	int& res = v[n];
	if (v[n] != -1) return res;
	if (n == 4 or n == 3) return res = 2;
	if (n == 2 or n == 1) return res = 1;
	if (n % 2 != 0) return res = 2 * x_algo(n/2 + 1) - 1; // Comptem el del mig dos cops i doncs, restem 1
	if (n % 2 == 0) return res = x_algo(n/2) + x_algo(n/2 + 1) - 1;
	return 0;
}


// Retorna el nombre minim de parets que cal per encabir x persones en n llocs
int f(int n, int x) {  // x <= n
	int& res = W[n][x];	
	if (res != -1) return res;
	if (x == 0) return res = 0;
	if (n == 0 or n == 1) return res = 0;  // base case, no available walls
	if (x_algo(n) >= x) return res = 0;  // provem cas que sigui optim sense cap paret
	int minim = n;  // init with worst case;
	for (int k = 1; k <= n/2; k++) {  // separem en dos parts l'array de n, en la posicio k; per simetria nomes cal fer fins la meitat
		int x1_min = x_algo(k);  // com s'omple segons l'algoritme la primera part
		int x2_min = x_algo(n-k);  // com s'omple segons l'algoritme la segona part
		/*A continuacio recorrem totes les maneres de forçar les x persones
		 * en les dues parts: x1 i x2. Sabem que com a minim han de anar ximin a
		 * la part i. per tant ximin <= xi. I ara tenim que x1 + x2 = n.
		 * per tant, x1min <= x1 <= n-x2min. la x2 ve fixada pel valor de x1. */
		for (int x1 = x1_min; x1 <= min(x - x2_min, k); x1++) {
			if ((x1 <= k) and (x - x1 <= n - k)) {
				minim = min(minim, 1 + f(k, x1) + f(n - k, x - x1));  // la paret del mig
			} 
		}
	}
	return res = minim;
}


int ceil(double x) {
	int ans = int(x);
	if (x - ans > 0) ans += 1;
	return ans;
}


int main() {
	// Init memory vectors for dynamic minim = min(minim, f(k, programming
	v = VI(500 + 1, -1);  
	W = VVI(501, VI(501, -1));
	while (cin >> n >> p) {
		int x = ceil(n*p/100); // nombre de persones necessaries per encabir a x;
		cout << f(n, x) << endl;
	}
	return 0;
}


/*
int main() {
	// Init memory vectors for dynamic minim = min(minim, f(k, programming
	v = VI(500 + 1, -1);  
	W = VVI(501, VI(501, -1));
	int x;
	while (cin >> n >> x) {
		cout << f(n, x) << endl;
		
	}
	return 0;
}
* */


	
/* Note that, given an array of available sites, precission on total array
 * is given by prec_tot = x/n = prec1 * n1/n + prec2 * n2/n; where
 * n1 and n2 are lenghts of subarrays (total array divided by wall between subarrays)
 * x1 and x2 are the number of occupancies in each subarray; n is the total 
 * array length. And prec1 and prec2 are the preccision on each subarray:
 * that is, prec_i = xi/ ni. 
 */

