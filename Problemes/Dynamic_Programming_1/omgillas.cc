#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
using ll = long long;
using VI = vector<int>;
using Vl = vector<ll>;
using VVl = vector<Vl>;
using VVVl = vector<VVl>;

VI coins = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};  // Fixed coins
int c, v;

VVVl M;

// Retorna el nombre de obtenir v amb c monedes fent servir fins la i-essima moneda
ll f(int v, int c, int i) {
	cout << v << ' ' << c << ' ' << i << endl;
	if (v < 0 or c < 0 or i < 0) return 0;
	ll& res = M[v][c][i-1];

	if (res != -1) return res;
	/*
	if (v < coins[i-1]) {
		for (int j = i-1; j >= 1; j--) {
			if (v >= coins[j-1]) return res = f(v, c, j);
		}
	}				
	*/
	if (v == 0 and c == 0 and i == 0) {cout << 1 << endl; return res = 1;}
	if (v == 0 or c == 0 or i == 0) {cout << 0 << endl; return res = 0;}
	res = f(v, c, i-1) + f(v-coins[i-1], c-1, i);	
	cout << res << endl;
	return res;
}



int main() {
	int n;

	cin >> n;
	M = VVVl(1e6, VVl(20, Vl(10, -1)));
	while (n > 0) {
		cin >> c >> v;
		cout << f(v, c, 10) << endl;	
		n--;
	}
	return 0;
}
