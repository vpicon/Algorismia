#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;



int n;
VI t; // t[i] gives t_i
VI v;

// Gives the maximum number of dealers hit, if we hit last dealer i.
int f(int i) {
	int& res = v[i];
	if (res != -1) return res;
	if (i == 0) return 1;
	int maximum = 1;
	for (int j = 0; j < i; j++) {
		if (t[i] - t[j] >= 10) maximum = max(maximum, 1 + f(j));
	}
	return res = maximum;
}
	



int main() {
	int cases;
	cin >> cases;
	while (cases > 0) {
		cin >> n;
		t = VI(n); 
		for (int i = 0; i < n; i++) cin >> t[i];
		
		v = VI(n, -1);
		int maximum = 0;
		for (int i = 0; i < n; i++) {  // we dont know if maximum sequence will end on t[n-1]
			maximum = max(maximum, f(i));
		}
		cout << maximum << endl;
		--cases;
	}
	return 0;
}
