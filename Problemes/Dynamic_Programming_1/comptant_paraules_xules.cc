#include <vector>
#include <iostream>

using namespace std;
using ll = long long;
using Vl = vector<ll>;
using VVl = vector<Vl>;

int n, v;
VVl M;  // Matriu de memoria

// Retorna el nombre de paraules amb n lletres i v vocals
ll f(int n, int v) {
	if (n < 0 or v < 0) return 0;
	ll& res = M[n][v];
	if (n == 0 and v == 0) return res = 1;
	if (n < v) return res = 0;
	if (n == 1 and v == 0) return res = 21;
	if (n == 1 and v == 1) return res = 5;
	if (n == 2 and v == 0) return res = 0;
	return res = 5 * f(n - 1, v - 1) + 105 * f(n - 2, v - 1);	
}

int main() {
	M = VVl(15+1, Vl(15+1, -1));
	while (cin >> n >> v) {
		cout << f(n, v) << endl;
	}
	return 0;
}
