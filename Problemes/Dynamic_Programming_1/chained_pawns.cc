#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

int r, c;
VVI M;


// Number of ways of putting chained pawns, starting at i, j
int f(int i, int j) {
	if (i < 0 or i >= r or j >= c or j < 0) return 0;
	int& res = M[i][j];
	if (res != -1) return res;
	if (i == r - 1) return res = 1;
	return res = f(i + 1, j - 1) + f(i + 1, j + 1);
}



int main() {
	while (cin >> r >> c) {
		M = VVI(r, VI(c, -1));
		int sum = 0;
		for (int j = 0; j < c/2; j++) {
			sum += 2*f(0, j);
		}
		if (c%2 == 1) sum += f(0, c/2);
		cout << sum << endl;
	}
	return 0;
}
