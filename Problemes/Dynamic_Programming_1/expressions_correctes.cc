#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

const int N = 25+1;

int n, m;
VVI M = VVI(N, VI(N, -1));




void escriu() {
	for (int i = 0; i < 25; i++) {
		for (int j = 0; j < 25; j++) cout << M[i][j] << ' ';
		cout << endl;
	}
}


/* Calcula el nombre de expressions de longitud n amb m variables */
int expr(int n, int m) { 
	int& res = M[n][m];
	if (res != -1) return res;
	if (n == 1) return res = m;
	if (n % 2 == 0) return res = 0;
	int p = expr(n-2, m); // expresions que ultima expresio es parentesi
	int s = 0; // expressions que ultima expresio es una resta
	if (n >= 7) {
		for (int i = 0; i <= (n-7)/2; i++) 
			s += expr(2*i + 1, m) * expr(n-6 - 2*i, m);
	}
	return res = p + s;
}


int main() {
	while (cin >> n >> m) {
		cout << expr(n, m) << endl;
		
	}
	return 0;
}
	
