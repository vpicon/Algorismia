#include <vector>
#include <iostream>

using namespace std;

using VI = vector<int>;
using VVI = vector<VI>;


VVI P; // Matriu de pixels
VVI M; // Matriu de sumes de vermells (no cal fer la mitjana)



int main() {
    int w, h, c;
	cin >> w >> h >> c;
	P = VVI(h, VI(w));
	for (int i = 0; i < h; i++) { // input matriu P
		for (int j = 0; j < w; j++) cin >> P[i][j];
	}
	
	M = VVI(h - c + 1, VI(w - c + 1));
	int imax = 0;
	int jmax = 0;
	int max = 0;
	
	/* Omplim primera fila de M */
	for (int k = 0; k < w-c+1; k++) {
		int& sum = M[0][k];
		for (int i = 0; i < c; i++) { // calculem suma sobre quadrat
			for (int j = 0; j < c; j++) sum += P[i][k + j];
		}
		if (sum > max) max = sum, imax = 0, jmax = k;
	}
	
	/* Omplim primera columna de M */
	for (int k = 1; k < h-c+1; k++) {
		int& sum = M[k][0];
		for (int i = 0; i < c; i++) { // calculem suma sobre quadrat
			for (int j = 0; j < c; j++) sum += P[k + i][j];
		}
		if (sum > max) max = sum, imax = k, jmax = 0;
	}
	

	/* Omplim tota la matriu M */
	for (int i = 1; i < h-c+1; i++) {
		for (int j = 1; j < w-c+1; j++) {
			int& sum = M[i][j];
			sum = M[i-1][j] + M[i][j-1] - M[i-1][j-1];
			sum += P[i-1][j-1] + P[i+c-1][j+c-1] - P[i-1][j+c-1] - P[i+c-1][j-1];
			if (sum > max) max = sum, imax = i, jmax = j;
		}
	}
	cout << imax << ' ' << jmax << endl;
	return 0;
}
