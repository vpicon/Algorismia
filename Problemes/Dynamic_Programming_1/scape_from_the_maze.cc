#include <iostream>
#include <vector>

using namespace std;

using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;

const int N = 1e6;

int n, m;
VVI C;
VVC obstacles;

int camins(int i, int j) {
	if (i < 0 or j < 0 or (obstacles[i][j] == 'X')) return 0; // mirar que estem dins la matriu
	int& res = C[i][j];
	if (C[i][j] != -1) return res;
	if (i + j == 0) return res = 1; // estem a 0,0; 
	return res = camins(i-1, j) + camins(i, j-1);	
}


int main() {
	C = VVI(41, VI(41, -1));
	
	cin >> n >> m;
	while (n + m != 0) {
		obstacles = VVC(n, VC(m));
		/* read obstacles */
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) cin >> obstacles[i][j];
		}
		
		int c = camins(n-1, m-1);
		if (c >= 1e6) cout << "!!!" << endl;
		else cout << c << endl;	
		cin >> n >> m;
	}
	return 0;
}





