#include <iostream>
#include <vector>

using namespace std;
using ll = long long;

ll m;
int n;

const int N = 1000 + 1;  // maxima longitud del vector de memoria

vector<ll> v;

ll fib(int n) {
    ll& res = v[n];
	if (v[n] != -1) return res;
	if (n == 0 or n == 1) return res = n;
	return res = (fib(n-1) + fib(n-2))%m;
}
	


int main() {
	while (cin >> n >> m) {
		v = vector<ll> (N, -1);
		cout << fib(n) << endl;
	}
}
