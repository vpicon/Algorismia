#include <iostream>
#include <vector>

using namespace std;
using ll = long long;
using Vll = vector<ll>;
using VVll = vector<Vll>;

int z, u;
VVll A, B;



ll b(int z, int u) {
	if (z < 0 or u < 0) return 0;
	ll& res = B[z][u];
	if (res != -1) return res;
	if (u == 0) return res = 1;
	if (u == 1) return res = 1;
	if (z == 0 and u == 2) return res = 1;
	if (z == 0 and u == 3) return res = 1;
	if (z == 1 and u == 2) return res = 2;
	if (z == 1 and u == 3) return res = 2;
	return res = b(z - 1, u - 2) + b(z - 2, u - 3);
}

/*
ll a(int z, int u) {
	return b(z - 1, u);
	if (z < 0 or u < 0) return 0;
	ll& res = A[z][u];
	if (res != -1) return res;
	if (z == 0) return res = 1;
	if (z == 1) return res = 1;
	
}
*/

int main() {
	A = VVll(91, Vll(91, -1));
	B = VVll(91, Vll(91, -1));
	while (cin >> z >> u) cout << b(z - 1, u) + b(z, u) << endl;
	return 0;
}
