#include <vector>
#include <iostream>

using namespace std;

using VI = vector<int>;

int n, q;
VI bolets, suma_bolets;

int main() {
    int k = 1;
    while (cin >> n) {
        cout << '#' << k << endl;
        bolets = suma_bolets = VI(n);
        
        for (int i = 0; i < n; i++) cin >> bolets[i];
        for (int i = 0; i < n; i++) {
            if (i == 0) suma_bolets[i] = bolets[i];
            else suma_bolets[i] = bolets[i] + suma_bolets[i-1];
        }
        cin >> q;
        int i, f;
        for (int k = 1; k <= q; k++) {
            cin >> i >> f;
            i--; f--; // canvi de format per a index de vectors
            if (i > f) swap(i,f);
            if(i == 0) cout << suma_bolets[f] << endl;
            else cout << suma_bolets[f] - suma_bolets[i-1] << endl;
        }
        k++;
    }
}
