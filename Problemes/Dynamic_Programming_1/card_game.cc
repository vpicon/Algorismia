#include <iostream>
#include <vector>

using namespace std;

const int N = 500000 + 1;

int n;
vector<int> v(N, -1);


int game(int n) {
	if (n < 0) return 0;
	int& res = v[n];
	if (res != -1) return res;
	if (n == 0) return res = 0;
	if (n < 5) return res = n;
	if (n < 8) return res = min(game(n-1), game(n-5)) + 1;
	if (n < 14) return res = min(min(game(n-1), game(n-5)), game(n-8)) + 1;
	return res = min( min(game(n-1), game(n-5)), 
					  min(game(n-8), game(n-14)) ) + 1;
}



int main() {
	while (cin >> n and n != -1) {
		cout << game(n) << endl;
	}
	return 0;
}
