#include <vector>
#include <iostream>
#include <string>

using namespace std;

using VI = vector<int>;
using VVI = vector<VI>;

const int N = 500 + 1;

VI v = VI(26, 1); // valors de les lletres

VVI M = VVI(N+1, VI(N+1, -1));


int main() {
	for (int i = 1; i < 26; i++) v[i] = v[i - 1] + (i + 1);// genera v
	
    string s;
    
    for (int i = 0; i < N+1; i++) M[i][0] = 0;
    for (int j = 0; j < N+1; j++) M[0][j] = 0;
    
    int i = 1;
    while (cin >> s) {
		bool primer = true;
		for (int j = 0; j < (int)s.size(); j++) {
			int& res = M[i][j+1];
			
			if (primer) primer = false;
			else cout << ' ';
			
			int valor = v[s[j] - 'A'];
			res = valor + M[i][j-1+1] + M[i-1][j+1] - M[i-1][j-1+1];
			
			cout << res;	
		}
		cout << endl;
		i++;
	}
	
	return 0;
}
    
