#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

int N, M;
VI A, B;
VVI D;

/* The problem is similar to the minimum edition problem, with now
 * minimizing the numeber of components of A and B which we dismiss */
int f(int i, int j) {
    if (i < 0 or j < 0) return 0;
    int& res = D[i][j];
    if (res != -1) return res;
    if (A[i] == B[j]) return res = 1 + f(i - 1, j - 1);
    return res = max(f(i, j - 1), f(i - 1, j));    
}


int main() {
    while (cin >> M >> N) {
        A = VI(M);
        B = VI(N);
        for (int i = 0; i < M; i++) cin >> A[i];
        for (int i = 0; i < N; i++) cin >> B[i];
        D = VVI(M, VI(N, -1));
        cout << f(M - 1, N - 1) << endl;
    }
    return 0;
}
 
