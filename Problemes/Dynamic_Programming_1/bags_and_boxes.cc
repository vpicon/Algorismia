#include <iostream>
#include <vector>

using namespace std;
using ll = long long;
using Vl = vector<ll>;
using VVl = vector<Vl>;

int n, x, y;
VVl C;
VVl M;


// Use the recurrence c(n, k) = c(n, k-1) + c(n-1, k-1);
ll c(int n, int k) {
	if (k < 0 or k > n) return 0;
	ll& res = C[n][k];
	if (res != -1) return res;
	if (k == n or k == 0) return res = 1;
	return res = c(n - 1, k) + c(n - 1, k - 1);
}

ll f(int n, int y) {
	if (n < 0 or y < 0) return 0;
	ll& res = M[n][y];
	if (res != -1) return res;
	if (n == 0 and x == 0) return res = 1;
	if (n < y) return res = 0;
	if (n == y) return res = 1;
	return res = y * f(n - 1, y) + f(n - 1, y - 1);
	/* Put the last object in other boxes with other objects,
	 *  or put the last object alone in a box */
}


int main() {
	M = VVl(26, Vl(26, -1));
	C = VVl(26, Vl(26, -1));
	while (cin >> n >> x >> y) {  // n distinctive objects, x identical bags, y boxes
		cout << c(n, y) * f(n - y, x) << endl;		
	}	
	return 0;
}

