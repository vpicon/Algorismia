#include <vector>
#include <iostream>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;

VVC T;
VVI M;

int n, m;

// Retorna el nombre de maneres de sortir del tauler des de (i j)
int f(int i, int j) { 
	if (i < 0 or j < 0) return 1; // Hem sortit del tauler
	int& res = M[i][j];
	if (res != -1) return res;
	char c = T[i][j];  // Casella actual;
	if (c == '*') return res = 0;
	if (c == 'R') return res = f(i, j-1) + f(i-1, j) + f(i-1, j-1); 
	if (c == 'C') return res = f(i-2, j-1) + f(i-1, j-2);	
}





int main() {
	while (cin >> n >> m) {
		M = VVI(n, VI(m, -1));  // Matriu de resultats
		T = VVC(n, VC(m));  // Tauler: Matriu de caracters
		// Llegeix tauler
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) cin >> T[i][j];
		}
		cout << f(n-1, m-1) << endl;
	}
	return 0;
}
