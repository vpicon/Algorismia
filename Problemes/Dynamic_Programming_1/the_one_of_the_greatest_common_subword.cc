#include <iostream>
#include <vector>
#include <string>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

string a, b;
int n, m; // size of a and b.
VVI D;


string par(int imax, int lmax) {
	string par;
	int istart = imax - lmax + 1;
	par = a.substr(istart, lmax);
	return par;
}


// Retorna la long de la paraula mes llarga que acaba en a[i] ( =  b[j]);
int f(int i, int j) {
    if (i < 0 or j < 0) return 0;
    int& res = D[i][j];
    if (res != -1) return res;
    if (i == 0 or j == 0) {
		if (a[i] == b[j]) return res = 1;
		else return res = 0;
	}
    if (a[i] == b[j]) return res = 1 + f(i-1, j-1);
    return res = 0;
}
    



int main() {
    while (cin >> a >> b) {
        n = (int)a.size();
        m = (int)b.size();
        
		
		D = VVI(n, VI(m, -1));
		int imax = 0, jmax = 0, lmax = 0;
		string parmax;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (f(i, j) > lmax) {
					lmax = f(i, j), imax = i, jmax = j;
					parmax = par(imax, lmax);
					
				}
				
				else if (f(i,j) == lmax and par(i, lmax) < parmax) {
					imax = i, jmax = j;
					parmax = par(imax, lmax);
				}
			}
		}
		if (lmax > 0) cout << parmax;
		cout << endl;
	}
    return 0;
}
