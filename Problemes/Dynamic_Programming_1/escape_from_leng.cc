#include <vector>
#include <iostream>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VVVI = vector<VVI>;
using VB = vector<bool>;
using VVB = vector<VB>;

/* Global Variables initialization */
int R, C, L;
int i_s, j_s;  // starting position
int i_f, j_f;  // finish position
VVB M;  // M[i][j] (map): marks true if position is clear, false if danger
VVVI P;   //P[i][j][l] saves f(i,j,l)



// Returns if (i, j) falls inside the map
bool in(int i, int j) {
	return (0 <= i and i < R) and (0 <= j and j < C);
}


// Gien the position of an Old One marks on the map dangerous zones
void mark_Old_One(int i_O, int j_O) {  // (i_O, j_O) position of Old One
	for(int i = -2; i <= 2; i++) {
		for(int j = -2; j <= 2; j++) {
			if ( in(i_O + i, j_O + j) ) M[i_O + i][j_O + j] = false;  // checks first valid position
		}
	}
	return;
}


// gives number of paths ending in i, j having at most L lateral movements
int f(int i, int j, int L) {
	if (not in(i, j) or L < 0) return 0;
	int& res = P[i][j][L];
	if (res != -1) return res;
	if (not M[i][j]) return 0; // M[i][j] dangerous cell
	if (i == i_s and j == j_s) return res = 1;
	res = f(i-1, j, L);
	if (L > 0) {
		for (int j_p = 0; j_p < C; j_p++) {
			if (j_p != j) {
				if (M[i][j_p]) res += f(i-1, j_p, L-1);
		}
	}
	return res;
}


int main() {
	while (cin >> R >> C >> L and not (R + C + L == 0)) {
		M = VVB(R, VB(C, true));
		P = VVVI(R, VVI(C, VI(L+1, -1)));
		
		// Read the map.
		char c;
		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				cin >> c;
				if (c == 'O') mark_Old_One(i, j);
				else if (i == 0 and c == 'D') i_s = i, j_s = j;
				else if (i == R-1 and c == 'P') i_f = i, j_f = j;
			}
		}

		int paths = f(i_f, j_f, L);
		if (paths == 0) cout << "bye" << endl;
		else cout << paths << endl;
		
		cout << endl;
		
		cout << "Pruebas : " << endl;
		for(int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				cout << P[i][j][1] << ' ';
			}
			cout << endl;
		}
		cout << endl;

	}
	return 0;
}
