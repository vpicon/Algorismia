#include <vector>
#include <iostream>

using namespace std;

using ll = long long;
using VL = vector<ll>;
using VVL = vector<VL>;

VVL M = VVL(31, VL(31, -1));


ll comb(int n, int k) {
    ll& res = M[n][k];
    if (res != -1) return res;
    if (k == 0 or k == n) return res = 1;
    return res = comb(n-1, k-1) + comb(n-1, k);   
}


int main() {
    int n, k;
    while (cin >> n >> k) cout << comb(n, k) << endl;
    return 0;
}
