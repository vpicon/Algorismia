#include <iostream>
#include <vector>
#include <string>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;
using VB = vector<bool>;
using VVB = vector<VB>;

int W, L; // v size bound
VVC M; // map
VVI P; // P[i][j] has the minimum number of strokes starting at (i,j)
VVB B;  // B[i][j] checks if there is a path starting from i,j 

int min3(int x, int y, int z) {
	return min(min(x, y), z);
}


bool f(int i, int j) { // returns if possible solution starting at (i,j).
	if (j < 0 or j >= W or i >= L or i < 0) return false; // check we are inside the map  
	if (P[i][j] != -1 or not B[i][j]) return B[i][j];
	if (M[i][j] == '*') return B[i][j] = false;
	
	int trap = 0;
	if (M[i][j] == 'T') trap += 3;
	
	if (i == L-1 and M[i][j] != '*') {
		P[i][j] = 0 + trap;
		
		return B[i][j] = true;
	}
	
	int minim=0;
	if (f(i+1, j-1) or f(i+1, j) or f(i+1, j+1)) {
		if (f(i+1, j-1)) {
			minim = P[i+1][j-1]+1;
			if (f(i+1, j)) minim = min(minim, P[i+1][j]);
			if (f(i+1, j+1)) minim = min(minim, P[i+1][j+1]+1);
		}
		else if (f(i+1, j)){
			minim = P[i+1][j];
			if (f(i+1, j+1)) minim = min(minim, P[i+1][j+1]+1);
		}
		else if (f(i+1, j+1)) minim = P[i+1][j+1]+1;
		P[i][j] = minim + trap;
		return B[i][j] = true;
	}
	else return B[i][j] = false;
}
	



int main() {
	cin >> W >> L;
	M = VVC(L, VC(W));
	P = VVI(L, VI(W, -1));
	B = VVB(L, VB(W, true));
	int i_start = 0;
	int j_start = 0;

	for (int i = 0; i < L; i++) {
		for (int j = 0; j < W; j++) {
			cin >> M[i][j];
			if (M[i][j] == 'M') i_start = i, j_start = j;
		}
	}
	
	
	
	if (f(i_start, j_start)) cout << P[i_start][j_start] << endl;
	else cout << "IMPOSSIBLE" << endl;
	
	return 0;
}

