#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
using Bool = int;  // 0/1 bool integer, with -1 for non checked in S
using VI = vector<int>;
using VB = vector<Bool>;
using VVB = vector<VB>;



int n;
VI c;  // coins vector
VI v; // used coins vector
VVB S;  // dynamic programming matrix if solvable A,i


void escriu() {
	bool primer = true;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < v[i]; j++) {
			if (primer) primer = false;
			else cout << ',';
			cout << c[i];
		}
	}
}


// Greater order
bool inverse(int x, int y) {
	return (x > y);
}


// Says if solution to A using coin i;
Bool f(int A, int i) {
	if (A < 0) return 0;
	if (A == 0) return 1;  // true
	if (i == n) return 0;  // false: no more coins available and still A left
	Bool& res = S[A][i];
	if (res != -1) return res;
	res = 0;  // init res as false
	int j = A/c[i];
	while (not res and j >= 0) {
		
		if( f(A - j*c[i], i + 1) ) {
			v[i] = j;
			return res = 1; // true
		}
		j--;
	}
	v[i] = 0;
	return res;	
}

int main () {
	while (cin >> n) {
		v = VI(n, 0);
		c = VI(n);
		
		for (int i = 0; i < n; i++) cin >> c[i];
		std::sort(c.begin(), c.end(), inverse);
		int A;
		cin >> A;
		
		S = VVB(A+1, VI(n, -1));  // 0, 1 bool solution matrix 
		
		bool solution = false;
		int i = 0;
		
		while (not solution and i < n) {
			solution = solution or f(A, i);
			i++;
		}
		
		i--;

		if (solution) escriu();
		else cout << -1;
		cout << endl;
		
	}
	return 0;
}
