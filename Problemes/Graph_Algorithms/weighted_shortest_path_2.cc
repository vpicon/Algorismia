#include <iostream>
#include <vector>
#include <queue>
#include <limits.h>
#include <stack>

using namespace std;
using VI = vector<int>;
using P = pair<int, int>;
using VP = vector<P>;
using VVP = vector<VP>;

const int INF = INT_MAX;

int n, m;
VVP G;  // adjacency list of pairs (w, v)


// calculates minimum distance from u to v (if possible) via dijkstra
void dijkstra(int u, int v) { 
	VI dist(n, INF); // init as infinite
	priority_queue<P> pending; // priority queue of peding vertices to visit
	
	dist[u] = 0;
	pending.push(P(0, u));
	
	VI predecesor(n, INF); // vector of predecessors: init at INFTY
	predecesor[u] = u;
	
	while (not pending.empty()) {
		P p = pending.top(); pending.pop(); 
		int x = p.second;
		int d = -p.first; 
		if (dist[x] == d) { // check if it is the latest entry to the pq of x 
			for (P arc : G[x]) {
				int c = arc.first;  // cost from x to y
				int y = arc.second; 
				int d2 = d + c;
				if (d2 < dist[y]) { // suboptimal path till the moment
					dist[y] = d2;
					pending.push(P(-d2, y));
					predecesor[y] = x;
				}
			}
		}
	}
	
	if (dist[v] == INF) {  // no path case
		cout << "no path from " << u << " to " << v << endl;
	}
	else {
		stack<int> path;
		int pred = v;
		while (pred != u) {
			path.push(pred);
			pred = predecesor[pred];
		}
		cout << u;
		while (not path.empty()) {
			cout << ' ' << path.top(); 
			path.pop();
		}
		cout << endl;
	}

}



int main() {
	while (cin >> n >> m) {
		G = VVP(n);
		while (m--) {
			int x, y, w;
			cin >> x >> y >> w;
			G[x].push_back(P(w, y));
		}
		
		int u, v;
		cin >> u >> v;
		dijkstra(u, v);
	}
	return 0;
}
