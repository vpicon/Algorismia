#include <iostream>
#include <vector>
#include <queue>

using namespace std;
using VC = vector<char>;
using VVC = vector<VC>;
using P = pair<int, int>;
using VI = vector<int>;
using VVI = vector<VI>;


int n, m;
VVC M;

VI m_i {1, -1, 0, 0};  // movement i
VI m_j {0, 0, 1, -1};  // movement j

bool pos_ok(int i, int j) {
    return (0 <= i and i < n and 0 <= j and j < m);
}


P bfs(const VVC& M) {// posicio del telecos donada
    queue<P> pending;  // vector de vertexs pendents
    VVI dist(n, VI(m, -1));  // dist[i][j] dona dist minima fins a (i,j)
    VVI pers(n, VI(m, -1));  // pers[i][j] dona nobre maxim de persones fins a (i,j) donaat dist minima
	
    /* Ens imaginem com problema de optimitzacio: tenim pair (distancia, persones)
     * primer minimitzem la distancia, despres dels que empaten, maximitzem
     * el nombre de persones
     */ 

    pending.push(P(0,0));  // init
    dist[0][0] = 0;
    if (M[0][0] == 'P') pers[0][0] = 1;
    else pers[0][0] = 0;


    while (not pending.empty()) {
        P p = pending.front(); pending.pop();
        int i = p.first;
        int j = p.second;
        
        if (M[i][j] == 'T') return P(dist[i][j], pers[i][j]);

        for (int k = 0; k < 4; k++) {  // iterate over all movements
            int n_i = i + m_i[k];
            int n_j = j + m_j[k];
            if ( pos_ok(n_i, n_j) and M[n_i][n_j] != '#' ) {
				int ocup = int(M[n_i][n_j] == 'P');  // 1 si hi ha una persona 0 si no
				if (dist[n_i][n_j] == -1) {
					dist[n_i][n_j] = dist[i][j] + 1;
					pers[n_i][n_j] = pers[i][j] + ocup;
					pending.push(P(n_i, n_j));
				}
				
				else if (dist[n_i][n_j] == dist[i][j] + 1) {
					pers[n_i][n_j] = max(pers[n_i][n_j], pers[i][j] + ocup);  // maxim del que hi havia o del cami que hem fet 
                }
			}
		}
	}
	
	
   
	// Hem acabat la cerca i no hem trobat telecos -> esta amagat
	cout << "El telecos esta amagat." << endl;
	return P(-1, -1);
}




int main()  {
    while (cin >> n >> m) {
        VVC M(n, VC(m));  // mapa
        bool telecos = false;
       
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cin >> M[i][j];
                if (M[i][j] == 'T') telecos = true;
            }
        }

        if (not telecos) cout << "El telecos ha fugit." << endl;
        else {  // sabem que hi ha un telecos al mapa
			P res = bfs(M);
			if (res.first != -1)
				cout << res.first << ' ' << res.second << endl;
        }
	}
       
	return 0;
}
