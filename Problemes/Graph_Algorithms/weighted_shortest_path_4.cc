#include <iostream>
#include <vector>
#include <queue>

using namespace std;
using VI = vector<int>;
using P = pair<int, int>;
using VP = vector<P>;
using VVP = vector<VP>;



const int INF = 1e9;

int n, m;
VVP G;  // adjecency list



// calculates minimum distance from u to v (if possible) via dijkstra
void dijkstra(int u, int v) { 
	VI dist(n, INF); // init as infinite
	priority_queue<P> pending; // priority queue of peding vertices to visit
	VI ways(n, 0);
	
	dist[u] = 0;
	ways[u] = 1;
	pending.push(P(0, u));
	
	while (not pending.empty()) {
		P p = pending.top(); pending.pop(); 
		int x = p.second;
		int d = -p.first; 
		if (dist[x] == d) { // check if it is the latest entry to the pq of x 
			for (P arc : G[x]) {
				int c = arc.first;  // cost from x to y
				int y = arc.second; 
				int d2 = d + c;
				if (d2 < dist[y]) { // suboptimal path till the moment
					dist[y] = d2;
					pending.push(P(-d2, y));
					ways[y] = ways[x];
				}
				else if (d2 == dist[y]) ways[y] += ways[x];
			}
		}
	}
	
	if (dist[v] == INF) {  // no path case
		cout << "no path from " << u << " to " << v << endl;
	}
	else cout << "cost " << dist[v] << ", " << ways[v] << " way(s)" << endl;
}


int main() {
	while (cin >> n >> m) {
		G = VVP(n); 
		while (m--) {  // Read adjacency list
			int u, v, c;
			cin >> u >> v >> c;
			G[u].push_back(P(c, v));
		}
		
		int x, y;
		cin >> x >> y;
		dijkstra(x, y);
	}
}
	
