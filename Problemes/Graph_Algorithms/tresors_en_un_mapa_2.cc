#include <iostream>
#include <vector>
#include <queue>


using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;
using P = pair<int, int>;

int n, m;
VVC M;

// returns true if i and j are inside M
bool ok(int i, int j) {
	return 0 <= i and i < n and 0 <= j and j < m;
}


int bfs_dist(int i0, int j0) {
	queue<P> pending;  // llista de vertexs pendents per visitar
	VVI dist(n, VI(m, -1));  // Vector de distancies
	
	dist[i0][j0] = 0;
	pending.push(P(i0, j0));

	while (pending.size() > 0) {  // bfs
		P p = pending.front(); pending.pop();  // position to visit
		int i = p.first;
		int j = p.second;
		
		if (M[i][j] == 't') return dist[i][j];  // hem trobat tresor
		else {  // mirem que no s'hagi visitat ja
			// visitem tots adjacents POSSIBLES de p
			if (ok(i - 1, j) and M[i - 1][j] != 'X' and dist[i - 1][j] == -1) {  // amunt
				pending.push(P(i - 1, j));
				dist[i - 1][j] = dist[i][j] + 1;
			}
			if (ok(i + 1, j) and M[i + 1][j] != 'X' and dist[i + 1][j] == -1) {  // avall
				pending.push(P(i + 1, j));
				dist[i + 1][j] = dist[i][j] + 1;
			}
			if (ok(i, j - 1) and M[i][j - 1] != 'X' and dist[i][j - 1] == -1) {  // esquerra
				pending.push(P(i, j - 1));
				dist[i][j - 1] = dist[i][j] + 1;
			}
			if (ok(i, j + 1) and M[i][j + 1] != 'X' and dist[i][j + 1] == -1) {  // dreta
				pending.push(P(i, j + 1));
				dist[i][j + 1] = dist[i][j] + 1;
			}
		}
	}

	return -1;  // no hem trobat cap tresor
}


int main() {
	cin >> n >> m;
	
	M = VVC(n, VC(m));
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++)
			cin >> M[i][j];
	
	int f, c;
	cin >> f >> c;
	
	int d = bfs_dist(f - 1, c - 1);
	if (d >= 0) 
		cout << "distancia minima: " << d << endl;
	else cout << "no es pot arribar a cap tresor" << endl;
}
	
	
