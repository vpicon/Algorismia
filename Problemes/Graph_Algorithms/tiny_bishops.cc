#include <iostream>
#include <vector>
#include <stack>
#include <string>

/* Observem que el problema té solució si la mida de la cada component 
 * connexa de la matriu donada, divideix el nombre de bishops de tal 
 * component connexa i tots aquests nombres son iguals per a totes les
 * components connexes (nomes cal que comprovem transitivament)  */
 
// Global definitions and variables
using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;
using P = pair<int, int>;

const int Null = -5;

int n, m;
VVI M;


// Returns if (i,j) lies inside the matrix nxm
bool ok(int i, int j) {
	return 0 <= i and i < n and 0 <= j and j < m;
}


// bishop movements
VI mov_i = {1, 1, -1, -1};
VI mov_j = {1, -1, 1, -1};
	
	
/* Returns result of dividing the bishops on the component of (i0, j0) 
 *  If such division is not possible return -1. It does it via dfs.
 * PRE: THere will allways be a bishop in (i0,j0) */
int bishops(int i0, int j0) {
	stack<P> pending;  
	int b = M[i0][j0];  // total number of bishops of the component
	int s = 1;  // size of the actual component;
	
	// init
	M[i0][j0] = Null;
	pending.push(P(i0, j0)); 
	
	// make dfs to search all the connected component
	while (not pending.empty()) {
		P p = pending.top(); pending.pop();
		int i = p.first; 
		int j = p.second;

		// augment size and bishop counters
		// look adjacent tiles
		for (int m = 0; m < 4; m++) {  // run over all bishop movements
			int n_i = i + mov_i[m];
			int n_j = j + mov_j[m];
			if (ok(n_i, n_j) and M[n_i][n_j] != Null) {
				b += M[n_i][n_j]; s++;
				M[n_i][n_j] = Null;
				pending.push(P(n_i, n_j));
			}
		}
	}

	if (b % s != 0) return -1;  // there is no way to divide the bishops
	else return b/s;

}
	


int main() {
	int t;  // cases
	cin >> t;
	int k = 1;  // current case
	while (k <= t) {
		cin >> n >> m;
		M = VVI(n, VI(m));
		
		// Read M
		string s;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cin >> s;
				if (s == "X") M[i][j] = Null;
				else M[i][j] = stoi(s);
			}
		}
		
		
		bool solved = false;  // says if we have components with different results
		int ant = -2;  // resultat de dividir els bishops de cada component del cas anterior
		// init ant as -2 for starting case
		
		int l = 0;
		while (l <= (n-1)*(m-1)) {
			int i = l/m;
			int j = l%m;
			if (M[i][j] != Null) {  // we have bishops to divide
				int r = bishops(i, j);

				if (r == -1) {
					solved = true;
					cout << "Case " << k << ": no" << endl;
					break;
				}
					
				if (ant == -2) ant = r;  // first case
				else if (r != ant) {  // case of no way to divide bishops in component and case of no equidistribution
					solved = true; 
					cout << "Case " << k << ": no" << endl;
					break;
				}
				
			}
			l++;
		}
		
		if (not solved) // yes case: we have ended in the last component
			cout << "Case " << k << ": yes" << endl;
		k++;
	}
	 
	 return 0;
 }
