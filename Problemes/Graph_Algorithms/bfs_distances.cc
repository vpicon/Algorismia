#include <iostream>
#include <vector>
#include <queue>
#include <limits.h>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

const int INF = INT_MAX;  // constant +infinit
int n, m;  // nombre de vèrtexs i nombre d'arestes del graf
VVI G;

VI bfs_dist(int x0) {
	VI dist = VI(n, INF);
	queue<int> pending;
	pending.push(x0);
	dist[x0] = 0;
	
	while (not pending.empty()) {
		int x = pending.front();
		pending.pop();
		for (int y : G[x]) {
			if (dist[y] == INF) {
				dist[y] = dist[x] + 1;
				pending.push(y);
			}
		}
	}
	return dist;
}
	
	
int main() {
	cin >> n >> m;
	
	G = VVI(n, VI());
	for (int arestes = 0; arestes < m; arestes++) {
		int x, y;
		cin >> x >> y;
		// sup graf no dirigit
		G[x].push_back(y);
		G[y].push_back(x);
	}
	int x0;
	cin >> x0;
	
	VI dist = bfs_dist(x0);
	for (int i = 0; i < n; i++) 
		cout << "distancia de " << i << " a " << x0 << " es : " << dist[i] << endl;
	
	return 0;
}
