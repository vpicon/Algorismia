#include <iostream>
#include <vector>

using namespace std;
using VC = vector<char>;
using VVC = vector<VC>;

int n, m;
VVC M;



int dfs(int i, int j) {
	if (i < 0 or i >= n or j < 0 or j >= m) return 0;
	if (M[i][j] == 'X') return 0;
	int res = 0;
	if (M[i][j] == 't') res += 1;
	M[i][j] = 'X';
	return res += dfs(i - 1, j) + dfs(i + 1, j) + dfs(i, j - 1) + dfs(i, j + 1);
}


int main() {
	cin >> n >> m;
	M = VVC(n, VC(m));
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++) 
			cin >> M[i][j];
	int f, c;
	cin >> f >> c;
	cout << dfs(f - 1, c - 1) << endl;
}
	
	
		
	
