#include <iostream>
#include <vector>

using namespace std;
using VC = vector<char>;
using VVC = vector<VC>;

int n, m;
VVC M;



bool dfs(int i, int j) {
	if (i < 0 or i >= n or j < 0 or j >= m) return false;
	if (M[i][j] == 'X') return false;
	if (M[i][j] == 't') return true;
	M[i][j] = 'X';
	return dfs(i - 1, j) or dfs(i + 1, j) or dfs(i, j - 1) or dfs(i, j + 1);
}


int main() {
	cin >> n >> m;
	M = VVC(n, VC(m));
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++) 
			cin >> M[i][j];
	int f, c;
	cin >> f >> c;
	cout << (dfs(f - 1, c - 1) ? "yes" : "no") << endl;
}
	
	
		
	
