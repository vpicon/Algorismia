#include <iostream>
#include <vector>
#include <queue>

using namespace std;
using VI = vector<int>;
using P = pair<int, int>;
using VP = vector<P>;
using VVP = vector<VP>;



const int INF = 1e9;

int n, m;
VVP G;  // adjecency list


void dijkstra(int x, int y) {
	VI weightdist(n, INF);
	VI dist(n, INF);
	priority_queue<pair<P, int>> pending;  // pending vertices to visit  by  ((wd, d), u)
	
	// init
	weightdist[x] = 0;
	dist[x] = 0;
	pending.push( pair<P,int> (P(0,0),x) );
	
	while (not pending.empty()) {
		pair<P,int> aux = pending.top(); pending.pop();
		int wd = -aux.first.first;  // opposite sign to achieve minimum in prio_q
		int d = -aux.first.second;
		int u = aux.second;
		
		if (weightdist[u] == wd and dist[u] == d) {  //  check for the most actual version of vertix to visit
			for (P arc : G[u]) {  // visit adjacent possible vertices
				int c = arc.first;
				int v = arc.second;
				int wd2 = c + wd;
				int d2 = d + 1;
				if (wd2 < weightdist[v]) {
					weightdist[v] = wd2;
					dist[v] = d2;
					pending.push( pair<P,int> (P(-wd2, -d2), v) );
				}
				else if (wd2 == weightdist[v] and d2 < dist[v]) dist[v] = d2;
			}
		}
	}
	if (weightdist[y] != INF) 
		cout << "cost " << weightdist[y] << ", " << dist[y] << " step(s)" << endl;
	else cout << "no path from " << x << " to " << y << endl;
}



int main() {
	while (cin >> n >> m) {
		G = VVP(n); 
		while (m--) {  // Read adjacency list
			int u, v, c;
			cin >> u >> v >> c;
			G[u].push_back(P(c, v));
		}
		
		int x, y;
		cin >> x >> y;
		dijkstra(x, y);
	}
}
	
