#include <iostream>
#include <vector>
#include <queue>

/* Since the roadmap graph is undirected, we can look for the arrival vertex
 * as if it was de arriving vertex in the dijkstra algorithm. */


using namespace std;
using P = pair<int, int>;
using VP = vector<P>;
using VVP = vector<VP>;
using VI = vector<int>;

const int INF = 1e9;

int n, m; 
VVP G;
VI cities;
// vectors given for the dijkstra algorithm
VI nearest_city;  // nearest_city[x] gives the nearest team of firefighters to city x
VI nearest_dist;  // nearest_dist[x] gives the minimum distance to a firefighter from city x


/* Calculates current shortest path tree via dijkstra algorithm, and 
 * actualizes values of nearest_city and nearest_dist vectors. */
void dijkstra (int x0) {  
	VI dist(n, INF);
	priority_queue<P> pending;  // pending vertices to visit
	
	// Init instances
	dist[x0] = 0;
	pending.push(P(0, x0));
	// Trivial actualization of solutions vectors
	nearest_dist[x0] = 0; nearest_city[x0] = x0;
	
	while (not pending.empty()) {
		P aux = pending.top(); pending.pop();
		int d = -aux.first;
		int x = aux.second;
		
		if (dist[x] == d) {
			for (P arc : G[x]) {
				int d2 = d + arc.first;  // cost = arc.first
				int y = arc.second;
				if (d2 < dist[y]) {
					dist[y] = d2;
					pending.push(P(-d2, y));
					if (dist[y] < nearest_dist[y]) {
						nearest_dist[y] = dist[y];
						nearest_city[y] = x0;
					}
				}
			}
		}
	}	
}
	




int main() {
	int cases; cin >> cases;
	for (int t = 1; t <= cases; t++) {
		cin >> n >> m;
		G = VVP(n);
		while (m--) {  // Read the adjacency list G
			int u, v, d;
			cin >> u >> v >> d;
			G[u].push_back(P(d, v));
			G[v].push_back(P(d, u));
		}
		
		 
		// Vectors calculated in the dijkstra algorightm
		nearest_city = VI(n);
		nearest_dist = VI(n, INF);
		int k; cin >> k;  // number of firefighter units
		while (k--) {
			int city;
			cin >> city;
			dijkstra(city);
		}
		
		
		cout << "Case #" << t << endl;  // Current case output line
		int q;  // queries
		while (q--) {
			int x; cin >> x;
			cout << "To get to " << x << ", distance "
				 << nearest_dist[x] << ", from city "
				 << nearest_city[x] << "." << endl;
		}
	}
	
}
