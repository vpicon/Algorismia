#include <iostream>
#include <vector>

using namespace std;


int n; 
vector<int> v;  // A l'element i te la columna j on es la reina i (de la fila i)
vector<bool> usat; //vector amb columnes usades

// Funció valor absolut
int abs(int x) {
	if (x < 0) return -x;
	return x;
}
	

void escriu() {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < v[i]; j++) cout << '.';
		cout << 'Q';
		for (int j = v[i] + 1; j < n; j++) cout << '.';
		cout << endl;
	}
	cout << endl;
}
	
	
// Retorna true si la reina a posicio (i0,j0) s'amenaca amb les i-1 anteriors
bool diagonal(int i0, int j0) {
	for (int i = 0; i < i0; i++) {
		if ( i0 - i == abs(j0 - v[i])) return true; //s'amenacen
	}
	return false;
}


void f(int i, bool& solucio) {  // i es la següent reina a probar.
	if (i == n) {
		escriu();
		solucio = true;
	}
	else if (i < n and not solucio) {
		for (int j = 0; j < n; j++) {
			if (not usat[j] and not diagonal(i, j)) {
				v[i] = j;
				usat[j] = true;
				f(i+1, solucio);
				usat[j] = false;
			}
		}		
	}
}


int main() {
	cin >> n;
	v = vector<int>(n);
	usat = vector<bool>(n, false);
	bool solucio = false;
	f(0, solucio);
	if (not solucio) cout << "NO SOLUTION" << endl;
}
		
