#include <iostream>
#include <vector>

using namespace std;
using ll = long long;


ll n, m;
int k;
vector<bool> v;
vector<int> M;

bool solution = false;


void escriu() {
	cout << n << " = ";
	cout << M[0];
	for (int i = 0; i < k-1; i++) {
		if (v[i]) cout << " + ";
		cout << M[i+1];
	}
	cout << endl;
}


void escriu_v() {
    for (int i = 0; i < (int)v.size(); i++) cout << v[i] << ' ';
    cout << endl;
}
		

int ndigits(ll m) { //passem m per copia
    int digits = 0;
    while (m > 0) {
        digits += 1;
        m /= 10;
    }
    return digits;
}


void crea_M(ll m) { //passem m per copia
	for (int i = 0; i < k; i++) {
		M[k - 1 - i] = m % 10;
		m /= 10;
	}
}



//cumsum es suma acumulada per un algo no sumat : quan no fiquem '+'
void numerology(int i, ll sumat, ll cumsum) { // i es quan portem del vector v; sumat es quant portem sumat a partir de i amb les sumes donades
    if (i == k-1) { 
        sumat += cumsum * 10 + M[i];
        if (sumat == n) {
            escriu();
            solution = true;
        }
	}
    else if (i < k-1 ) {
        cumsum = cumsum * 10 + M[i];
        
        v[i] = true;
        numerology(i+1, sumat+cumsum, 0);
        
        v[i] = false;
        numerology(i+1, sumat, cumsum);
    }
}




int main() {
    while (cin >> n >> m) {
        solution = false;
        
        k = ndigits(m);
		M = vector<int>(k); //crea vector amb tots els digits de m
		crea_M(m);
        
		v = vector<bool>(k - 1, true); //d'entre el nombre de digits de m - 1, en fiquem o no una suma

		numerology(0, 0, 0); // Better start at end, initialize with all sums activated
	
		if (not solution) 
			cout << "No solution for " << n << ' ' << m << '.' << endl;
	}
}
