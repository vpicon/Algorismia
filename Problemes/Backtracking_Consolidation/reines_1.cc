#include <iostream>
#include <vector>

using namespace std;


int n; 
vector<int> v;  // A l'element i te la columna j on es la reina i (de la fila i)
vector<bool> usat; //vector amb columnes usades

// Funció valor absolut
int abs(int x) {
	if (x < 0) return -x;
	return x;
}
	
	
	
// Retorna true si la reina a posicio (i0,j0) s'amenaca amb les i-1 anteriors
bool diagonal(int i0, int j0) {
	for (int i = 0; i < i0; i++) {
		if ( i0 - i == abs(j0 - v[i])) return true; //s'amenacen
	}
	return false;
}


int f(int i) {  // i es la següent reina a probar.
	if (i == n) return 1;
	else if (i < n) {
		int suma = 0;
		for (int j = 0; j < n; j++) {
			if (not usat[j] and not diagonal(i, j)) {
				v[i] = j;
				usat[j] = true;
				suma += f(i+1);
				usat[j] = false;
			}
		}
		return suma;		
	}
	return 0;
}

int main() {
	cin >> n;
	v = vector<int>(n);
	usat = vector<bool>(n, false);
	cout << f(0) << endl;
}
		
