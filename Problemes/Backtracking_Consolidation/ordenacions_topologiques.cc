#include <iostream>
#include <vector>

using namespace std;

using VVB = vector< vector<bool> >; 

int n, m;
vector<int> v;
vector<bool> usat;
VVB preced;  // preced[i][j] diu que element i ha de precedir al j

void escriu() {
	cout << v[0];
	for (int i = 1; i < (int)v.size(); i++) cout << ' ' << v[i];
	cout << endl;
	return;
}


bool posible(int j) {
	for (int i = 0; i < n; i++) { //recorrem totes les files de preced amb columna j
		if (preced[i][j] and not usat[i]) return false; //i ha de precedir a j0 pero i encara no ha sortit
	}
	return true;	
}



void ordenacio(int i) {
	if (i == n) escriu();
	else if (i == 0) {
		for (int j = 0; j < n; j++) { //recorrem tots els possibles nombres
			if (posible(j)) {
				usat[j] = true;
				v[0] = j;
				ordenacio(1);
				usat[j] = false;
			}
		}
	}
	else if (i < n) {
		for (int j = 0; j < n; j++) {
			if (not usat[j] and posible(j)) {
				usat[j] = true;
				v[i] = j;
				ordenacio(i + 1);
				usat[j] = false;
			}
		}
	}
	return;
}






int main() {
	cin >> n >> m; 
	v = vector<int>(n);  // Vector amb la ordenacio
	usat = vector<bool>(n, false);  // Vector amb nombres usats en v
	preced = VVB(n, vector<bool>(n, false));
	
	while (m > 0) {  // Llegim precedencies
		int a, b;
		cin >> a >> b;
		preced[a][b] = true;
		m--;
	}
	ordenacio(0);
	return 0;
}
	
