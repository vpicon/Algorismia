#include <iostream>
#include <vector>

using namespace std;
using VB = vector<bool>;
using VI = vector<int>;

int n, r;
VI v; // caselles amenacades. Representacio del tauler en vector n*n; indica quants reis lamenacen
VI R; // R[i] indica la posicio del rei i en el vector del tauler


void escriu_v(const VI& vec) {
	for (int i = 0; i < (int)vec.size(); i++) cout << vec[i] << ' ';
	cout << endl;
}

bool t(int i, int j) {
	return (0 <= i and i < n) and (0 <= j and j < n);
}
	


void amenaca(int k){
	int i = k/n; 
	int j = k%n;
	v[k]++;
	if ( t(i, j-1)   ) v[k-1]++;
	if ( t(i, j+1)   ) v[k+1]++;
	if ( t(i-1, j-1) ) v[k-1 - n]++;
	if ( t(i-1, j)   ) v[k - n]++;
	if ( t(i-1, j+1) ) v[k+1 - n]++;
	if ( t(i+1, j-1) ) v[k-1 + n]++;
	if ( t(i+1, j)   ) v[k + n]++;
	if ( t(i+1, j+1) ) v[k+1 + n]++;
	return;
}

void desamenaca(int k) {
	int i = k/n; 
	int j = k%n;
	v[k]--;
	if ( t(i, j-1)   ) v[k-1]--;
	if ( t(i, j+1)   ) v[k+1]--;
	if ( t(i-1, j-1) ) v[k-1 - n]--;
	if ( t(i-1, j)   ) v[k - n]--;
	if ( t(i-1, j+1) ) v[k+1 - n]--;
	if ( t(i+1, j-1) ) v[k-1 + n]--;
	if ( t(i+1, j)   ) v[k + n]--;
	if ( t(i+1, j+1) ) v[k+1 + n]--;
	return;
}


void escriu() {
	int k = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (R[k] ==  i*n + j) {
				cout << 'K';
				k++;
			}
			else cout << '.';
		}
		cout << endl;
	}
	cout << "----------" << endl;
	return;
}

void reis(int i) { //i es quants reis ja hem escrit
	if (i == r) escriu();
	else if (i == 0) {
		for (int j = 0; j < n*n; j++) {  // Revisar posicio final de j
			R[0] = j;
			amenaca(j);
			reis(i + 1);
			desamenaca(j);
		}

	}
	else if (i < r) {
		for (int j = R[i-1] + 2; j < n*n; j++) {  // recorrem tauler
			if (v[j] == 0) {  // Mirem si la posicio j esta disponible
				R[i] = j;
				amenaca(j);
				if (i == 2) escriu_v(R), escriu_v(v);
				reis(i + 1);
				desamenaca(j);
			}
		}
	}
	return;
}


int main() {
	cin >> n >> r;
	v = VI(n*n, 0); 
	R = VI(r, 0); 
	reis(0);
	return 0;
}
