#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VC = vector<char>;
using VVC = vector<VC>;


int n, m;  // floor dimensions
int i_s, j_s, i_f, j_f; //i, j start and i, j finish

VVC M;
VVI C; // 0 buit, 1 amunt, 2 dreta, 3 abaix, 4 esquerra

void escriu() {
	int i = i_s, j = j_s;
	while (i != i_f or j != j_f) {
		cout << M[i][j];
		if (C[i][j] == 1) i--; 
		else if (C[i][j] == 2) j++;
		else if (C[i][j] == 3) i++;
		else if (C[i][j] == 4) j--;

	}
	cout << M[i][j];
	cout << endl;
	return;
}

void escriu_C() {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) cout << C[i][j] << ' ';
		cout << endl;
	}
	cout << endl;
}


void camins(int i, int j) {  // i,j es la casella per la que estem fent el cami
	if (i == i_f and j == j_f) escriu();
	else if (true) { 
		if (i-1 >= 0 and C[i - 1][j] == 0) { //prova casella amunt buida, i que seguim dins matriu
			C[i][j] = 1; //posa cap amunt
			camins(i - 1, j);
			C[i][j] = 0; // torna a posar buida
		}
		if (j+1 <= m-1 and C[i][j + 1] == 0) { //prova casella dreta buida, i que seguim dins matriu
			C[i][j] = 2; //posa cap dreta
			camins(i, j + 1);
			C[i][j] = 0; // torna a posar buida
		}
		if (i+1 <= n-1 and C[i + 1][j] == 0) { //prova casella abaix buida, i que seguim dins matriu
			C[i][j] = 3; //posa cap abaix
			camins(i + 1, j);
			C[i][j] = 0; // torna a posar buida
		}
		if (j-1 >= 0 and C[i][j - 1] == 0) { //prova casella esquerra buida, i que seguim dins matriu
			C[i][j] = 4; //posa cap esquerra
			camins(i, j - 1);
			C[i][j] = 0; // torna a posar buida
		}
		
	}
	return;
}


int main() {
	cin >> n >> m;
	M = VVC(n, VC(m));
	C = VVI(n, VI(m, 0));
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) cin >> M[i][j];
	}
	cin >> i_s >> j_s >> i_f >> j_f;
	//i_s = n-1 - i_s; i_f = n-1 - i_f;  // canvi de format
	
	camins(i_s, j_s);
	
	return 0;
}
