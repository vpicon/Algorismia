#include <vector>
#include <iostream>

using namespace std;

int n;
vector<int> v; //permutacions de cos, cama, cos, cama ,...
vector<bool> usat_bodies;
vector<bool> usat_legs;


void escriu() {
	bool primer = true;
	for (int i = 0; i < 2*n; i += 2) {
		if (primer) primer = false;
		else cout << '-';
		int head = 'A';
		int legs_char = 'a';
		cout << char(head + i/2);
		cout << v[i];
		cout << char(legs_char + v[i+1]);
	}
	cout << endl;
}



void permutate(int i) {// generates permutations of legs
	if (i == 2*n) escriu(); //writes a permutation of legs, given a perm of bodies
	else if (i < 2*n) {
		for (int j = 0; j < n; j++) {
			if (i%2 == 0) { // permutem bodies
				if (not usat_bodies[j]) {
					v[i] = j;
					usat_bodies[j] = true;
					permutate(i + 1);
					usat_bodies[j] = false;
				}
			}
			else {
				if (not usat_legs[j]) {
					v[i] = j;
					usat_legs[j] = true;
					permutate(i + 1);
					usat_legs[j] = false;
				}
			}
		}
	}
}


	

int main() {
	int i = 1;
	while (cin >> n and n > 0) {
		cout << "Case " << i << endl;
		v = vector<int>(2*n);
		usat_bodies = vector<bool>(n, false);
		usat_legs = vector<bool>(n, false);
		permutate(0);
		i++;
	}
	return 0;
}
	
