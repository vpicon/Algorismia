#include <iostream>
#include <vector>

using namespace std;

using VI = vector<int>;
using VVI = vector<VI>;
using VB = vector<bool>;
using VVB = vector<VB>;


VVI sudoku;
bool solution;

VI suma_f = VI(9);  // conte emmagatzemat quant val la suma de files
VI suma_c = VI(9);  // conte emmagatzemat quant val la suma de columnes
VI suma_q = VI(9);  // conte emmagatzemat quant val la suma de quadrants
VVB usat_f = VVB (9, VB(9, false)); // usat_f[i][k] : si fila i ha usat el numero k
VVB usat_c = VVB (9, VB(9, false)); // usat_c[j][k] : si columna j ha usat el numero k
VVB usat_q = VVB (9, VB(9, false)); // usat_q[l][k] : si quadrant l ha usat el numero k



int quadrant(int i, int j) { // retorna al bloc de 9 que estem al sudoku
	return i % 3 + j % 3;
}

void read_sudoku() {
	char null;
	for (int i = 0; i < 9; i++) {
		if (i == 3 or i == 6) {
			for (int k = 0; k < 21; k++) cin >> null;
		}
		for (int j = 0; j < 9; j++) {
			if (j == 3 or j == 6) cin >> null;
			int k;
			cin >> k;
			sudoku[i][j] = k;
			if (k > 0) {
				usat_c[i][k-1] = true;
				usat_f[j][k-1] = true;
				usat_q[quadrant(i,j)][k-1] = true;  // falla
			}
		}
	}
}

void write_sudoku() {
	for (int i = 0; i < 9; i++) {
		if (i == 3 or i == 6) {
			for (int k = 0; k < 21; k++) {
				if (k == 6 or k == 14) cout << '+';
				else cout << '-';
			}
			cout << endl;
		}
		bool primer = true;
		for (int j = 0; j < 9; j++) {
			if (j == 3 or j == 6) cout << ' ' << '|';
			if (primer) primer = false;
			else cout << ' ';
			cout << sudoku[i][j];
		}
		cout << endl;
	}
}




void write_c() {
	for (int i = 0; i < 9; i++) {
		if (i == 3 or i == 6) {
			for (int k = 0; k < 21; k++) {
				if (k == 6 or k == 14) cout << '+';
				else cout << '-';
			}
			cout << endl;
		}
		bool primer = true;
		for (int j = 0; j < 9; j++) {
			if (j == 3 or j == 6) cout << ' ' << '|';
			if (primer) primer = false;
			else cout << ' ';
			cout << usat_c[i][j];
		}
		cout << endl;
	}
}

void write_f() {
	for (int i = 0; i < 9; i++) {
		if (i == 3 or i == 6) {
			for (int k = 0; k < 21; k++) {
				if (k == 6 or k == 14) cout << '+';
				else cout << '-';
			}
			cout << endl;
		}
		bool primer = true;
		for (int j = 0; j < 9; j++) {
			if (j == 3 or j == 6) cout << ' ' << '|';
			if (primer) primer = false;
			else cout << ' ';
			cout << usat_f[i][j];
		}
		cout << endl;
	}
}

void write_q() {
	for (int i = 0; i < 9; i++) {
		if (i == 3 or i == 6) {
			for (int k = 0; k < 21; k++) {
				if (k == 6 or k == 14) cout << '+';
				else cout << '-';
			}
			cout << endl;
		}
		bool primer = true;
		for (int j = 0; j < 9; j++) {
			if (j == 3 or j == 6) cout << ' ' << '|';
			if (primer) primer = false;
			else cout << ' ';
			cout << usat_q[i][j];
		}
		cout << endl;
	}
}





bool disponible(int i, int j, int k) {
	return (not usat_f[i][k] and not usat_c[j][k] and 
	not usat_q[quadrant(i,j)][k]);
}


void solve_sudoku(int i, int j) { //i es fila en que estem
	if (i == 9 and j == 9) {
		cout << "solved" << endl;
		
		write_sudoku();
		
		solution = true;
	}
	else if (i < 9 and j < 9) {
		for (int k = 0; k < 9; k++) { //numero a provar
			if (disponible(i,j,k)) {
				usat_f[i][k] = true; 
			    usat_c[j][k] = true;
			    usat_q[quadrant(i,j)][k] = true;
			    sudoku[i][j] = k;
			    j++;
			    solve_sudoku(i + j%9, j%9);
			    usat_f[i][k] = false; 
			    usat_c[j][k] = false;
			    usat_q[quadrant(i,j)][k] = false;
			}
		}
	}
	return;
}



int main() {
	int n;
	cin >> n;
	while (n > 0) {
		sudoku = VVI(9, VI(9));
		cout << n << endl;
		read_sudoku();
		solution = false;
		write_c();
		cout << endl;
		write_f();
		cout << endl;
		write_q();
		cout << endl;
		solve_sudoku(0, 0);

		cout << endl;
		
		n--;
	}
}
