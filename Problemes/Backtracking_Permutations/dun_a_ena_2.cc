#include <iostream>
#include <vector>

using namespace std;

int n;
vector<int> v;
vector<bool> usat; //vector amb nombres usades

void escriu() {
    cout << '(';
    bool primer = true;
    for (int i = 0; i < (int)v.size(); i++) {
        if (primer) primer = false;
        else cout << ',';
        cout << v[i] + 1;
    }
    cout << ')' << endl;
}
		



void permutacions(int i, int anterior) { 
	/* i indica el nombre de elements pel qual ja hem indicat quant val 
	la permutació (no quant hem escrit en el vector) */
	if (i == n-1) {
		v[anterior] = 0;
		escriu();
	}
	else if (i == 0) {
		for (int j = 1; j < n; j++) { //evitant el primer que sigui 0 (cicle trivial)
			v[0] = j;
			usat[j] = true;
			permutacions(1, j);
			usat[j] = false;
		}
	}
	else if (i < n-1) {
		for (int j = 1; j < n; j++) {
			if (not usat[j]) {
				v[anterior] = j;
				usat[j] = true;
				permutacions(i+1, j);
				usat[j] = false;
			}
		}
	}
}
			


int main() {
	cin >> n;
	v = vector<int>(n);
	usat = vector<bool>(n, false); 
	permutacions(0, 0);
}
	
