#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<int> v;
vector<bool> usat;
int n;
vector<string> paraules;

void escriu() {
    cout << '(';
    bool primer = true;
    for (int i = 0; i < (int)v.size(); i++) {
        if (primer) primer = false;
        else cout << ',';
        cout << paraules[v[i]];
    }
    cout << ')' << endl;
}

void permutations(int i) {
    if (i == n) escriu();
    else if (i < n) {
        for (int j = 0; j < n; j++) {
            if (not usat[j]) {
                usat[j] = true;
                v[i] = j;
                permutations(i+1);
                usat[j] = false;
            }
        }
    }
}
                
    

int main() {
    cin >> n;
    v = vector<int>(n);
    usat = vector<bool>(n, false);
    paraules = vector<string>(n);
    for (int i = 0; i < n; i++) cin >> paraules[i];
    permutations(0);
    return 0;
}
