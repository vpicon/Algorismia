#include <iostream>
#include <vector>
#include <string>

using namespace std;

int f, c;
vector<bool> usat;
vector<int> v;

void escriu() {
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < v[i]; j++) cout << '.';
        cout << 'R';
        for (int j = v[i] + 1; j < c; j++) cout << '.';
        cout << endl;
    }
    cout << endl;
}


void torres(int fila) { // v tractat fins fila
    if (fila == f) escriu();
    else if (fila < f) {
        for (int j = 0; j < c; j++) {
            if (not usat[j]) {
                v[fila] = j;
                usat[j] = true;
                torres(fila + 1);
                usat[j] = false;
            }
        }
    }
}


int main() {
    cin >> f >> c;
    usat = vector<bool>(c, false);
    v = vector<int>(f);
    torres(0);
    return 0;
}


