#include <iostream>
#include <vector>
#include <string>

using namespace std;

int n;
vector<char> vocals;
vector<char> consonants;
vector<bool> usat_v;
vector<bool> usat_c;
vector<char> v_v;
vector<char> v_c;

//sortida ha de ser e ordre alfabetic;

void escriu() {
    for (int i = 0; i < n; i++) {
        cout << v_c[i] << v_v[i];
    }
    cout << endl;
    return;   
}


void permutations (int i) {
	if (i == 2*n) escriu();
	else if (i < 2*n) {
		if (i % 2 == 0) { //permuta amb consonants
			for (int j = 0; j < n; j++) {
                if (not usat_c[j]) {
                    v_c[i/2] = consonants[j];
                    usat_c[j] = true;
                    permutations(i+1);
                    usat_c[j] = false;
                }
            }
		}
		else { // permuta vocals
			for (int j = 0; j < n; j++) {
                if (not usat_v[j]) {
                    v_v[i/2] = vocals[j];
                    usat_v[j] = true;
                    permutations(i+1);
                    usat_v[j] = false;
                }
            }
		}
	}
}

int main() {

    cin >> n;
    vocals = vector<char>(n);
    consonants = vector<char>(n);
    usat_c = vector<bool>(n, false);
    usat_v = vector<bool>(n, false);
    v_v = vector<char>(n);  // permutacio de vocals
    v_c = vector<char>(n);  // permutacio de consonants
    for (int i = 0; i < n; i++) cin >> consonants[i];
    for (int i = 0; i < n; i++) cin >> vocals[i];
    permutations(0);
    return 0;
}
