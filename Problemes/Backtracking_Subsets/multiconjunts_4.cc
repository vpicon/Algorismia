#include <iostream>
#include <vector>

using namespace std;

int n, t, x, y;
vector<int> sol; // vector de [buit,nombre de uns, nombre de dos,...]

void escriu() {
    cout << '{';
    bool primer = true;
    for (int i = 0; i <= n; ++i) {
        for (int j = 0; j < sol[i]; ++j) {
            if (primer) primer = false;
            else cout << ',';
            cout << i;
        }
    }
    cout << '}' << endl;
}


//i és la posició del següent a omplir; s la mida del multiconjunt escrit
void f(int i, int s) { 
    if (s > t) return;
    if (i == n+1) {
        if (s == t) escriu();
        return;
    }
    for (int v = x; v <= y; v++) {
        sol[i] = v;
        f(i+1, s+v);
    }
}


int main() {
    cin >> n >> x >> y >> t;
    sol = vector<int>(n+1);//posicio inicial sol(0) estará buida, posició i te element i
    f(1, 0);
}
