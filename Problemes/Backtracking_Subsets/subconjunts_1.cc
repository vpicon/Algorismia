#include <iostream>
#include <vector>
#include <string>

using namespace std;

int n;
vector<bool> v;
vector<string> paraules;


void escriu() {
	cout << '{';
	bool primer = true;
	for (int i = 0; i < n; i++) {
		if (v[i]) {
			if (primer) primer = false;
			else cout << ',';
			cout << paraules[i];
		}
	}
	cout << '}' << endl;
}


void f(int i) {
	if (i == n) escriu();
	else if (i < n) {
		v[i] = 0;
		f(i+1);
		v[i] = 1;
		f(i+1);		
	}
}


int main() {
	cin >> n;
	v = vector<bool>(n, false);
	paraules = vector<string>(n);
	for (int i = 0; i < n; i++) cin >> paraules[i];
	f(0);
	return 0;
}
