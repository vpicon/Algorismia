#include <iostream>
#include <vector>

using namespace std;

int n, s;
vector<int> x;
vector<bool> v;

void escriu() {
    cout << '{';
    bool primer = true;
    for (int i = 0; i < (int)x.size(); i++) {
        if (v[i]) {
            if (primer) primer = false;
            else cout << ',';
            cout << x[i];
        }
    }
    cout << '}' << endl;
}


void f(int i, int sumat) {
    if (sumat == s and i == n) escriu();
    if (i < n) {
		v[i] = false;
        f(i+1, sumat);
        v[i] = true;
        f(i+1, sumat + x[i]);
    }    
}

int main() {
    cin >> s >> n;
    v = vector<bool>(n);
    x = vector<int>(n);
    for(int i = 0; i < n; i++) cin >> x[i];  
    f(0,0);    
}
