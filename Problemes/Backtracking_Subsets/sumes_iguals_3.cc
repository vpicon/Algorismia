#include <iostream>
#include <vector>

using namespace std;

int n, s;
vector<int> x;
vector<bool> v;

void escriu() {
    cout << '{';
    bool primer = true;
    for (int i = 0; i < (int)x.size(); i++) {
        if (v[i]) {
            if (primer) primer = false;
            else cout << ',';
            cout << x[i];
        }
    }
    cout << '}' << endl;
}


void f(int i, int sumat, int reste) {
    if (sumat == s and i == n) escriu();
    if (i < n and sumat <= s) {
		reste -= x[i];
		v[i] = false;
        if (sumat + reste >= s) f(i+1, sumat, reste);
        v[i] = true;
        sumat += x[i];
        if (sumat + reste >= s) f(i+1, sumat, reste);
    }    
}

int main() {
    cin >> s >> n;
    v = vector<bool>(n);
    x = vector<int>(n);
    int reste = 0;
    for(int i = 0; i < n; i++) {
    cin >> x[i];
    reste += x[i]; //quant sumand queda al vector
	}
    f(0, 0, reste);    
}
