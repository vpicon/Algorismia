#include <iostream>
#include <vector>

using namespace std;

int n;
vector<int> v;

void escriu(const vector<int>& v) {
	for (int i = 0; i < (int)v.size()-1; i++) {
		cout << v[i] << ' ';
	}
	cout << v[n-1] << endl;	
} 

void f(int i) {
	if (i == n) {
		 escriu(v);
	}
	else {
		v[i] = 0;
		f(i+1);
		v[i] = 1;
		f(i+1);
	}
}

int main() {
	cin >> n;
	v = vector<int> (n);
	f(0);
}
	
	
