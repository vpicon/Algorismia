#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n, s;
vector<int> x;
vector<bool> v;


bool compara_gran(int x, int y) {
	return x > y;
}

void escriu_vector(const vector<int>& vec) {
	for (int i = 0; i < (int)vec.size(); i++) {
		cout << vec[i] << ' ';
	}
	cout << endl;
}


void escriu() {
    cout << '{';
    bool primer = true;
    for (int i = 0; i < (int)x.size(); i++) {
        if (v[i]) {
            if (primer) primer = false;
            else cout << ',';
            cout << x[i];
        }
    }
    cout << '}' << endl;
}

/*
void escriu() {
    bool primer = true;
    bool buit = true;
    for (int i = 0; i < (int)x.size(); i++) {
        if (v[i]) {
            if (primer) {
				primer = false;
				cout << '{';
				buit = false;
			}
            else cout << ',';
            cout << x[i];
        }
    }
    if (not buit) cout << '}' << endl;
    else cout << "no solution" << endl;
}
*/

void f(int i, int sumat, bool& escrit) {
    if (sumat == s and i == n) {
		escriu();
		escrit = true;
	}
    if (i < n) {
		if (not escrit) {
			v[i] = true;
			f(i+1, sumat + x[i], escrit);
        }
        if (not escrit) {
			v[i] = false;
			f(i+1, sumat, escrit);
		}
    }    
}

int main() {
    cin >> s >> n;
    v = vector<bool>(n);
    x = vector<int>(n);
    for(int i = 0; i < n; i++) cin >> x[i];
    sort(x.begin(), x.end(), compara_gran); 
    bool escrit = false;
    f(0, 0, escrit);    
    if (not escrit) cout << "no solution" << endl;
}
