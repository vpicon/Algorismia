#include <iostream>
#include <vector>
#include <string>

using namespace std;

int m, n;
vector<int> v; //vector de les paraules que escollim;
vector<string> paraules;



void escriu() {
	bool primer = true;
	cout << '{';
	for (int i = 0; i < m; i++) {
		if (primer) primer = false;
		else cout << ',';
		cout << paraules[v[i]];
	}
	cout << '}' << endl;
}

void f(int i) {  // construirem permutacions ordenades, per a descriure un conjunt: v0 < v1 < v2 < ... < vn-1
	if (i == m) escriu(); // i compta el nombre de paraules escollides
	else if (i == 0) { // cas especial
		for (int j = 0; j <= n-m; j++) { //no cal anar fins a n ja que que  sabem que si comencem per una j > n-m despres no podrem escriure un subconjunt de m elements
			v[0] = j;
			f(1);
		}
	}
	else if(i < m) { //and (v[i-1] + 1) < (m-i)) { // ens diu si podrem omplir tot el conjunt amb les paraules que queden 
		for (int j = v[i-1]+1; j <= n-m+i; j++) {
			v[i] = j;
			f(i+1);
		}
	}
}




int main() {
	cin >> m >> n;
	v = vector<int>(m,-1);
	paraules = vector<string>(n);
	for (int i = 0; i < n; i++) cin >> paraules[i];
	f(0);
	return 0;
}
