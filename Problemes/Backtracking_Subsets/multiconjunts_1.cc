#include <iostream>
#include <vector>

using namespace std;

int n, x;
vector<int> v; //v[i] indica cuants cops apareix el element i en el multiconjunt

void escriu() {
	bool primer = true;
	cout << '{';
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < v[i]; j++) {
			if (primer) primer = false;
			else cout << ',';
			cout << i+1;
		}
	}
	cout << '}' << endl;
}

void multiconjunts(int i) {
	if (i == n) escriu();
	else if (i < n) {
		for (int j = 0; j <= x; j++) {
			v[i] = j;
			multiconjunts(i+1);
		}
	}
}

int main() {
	cin >> n >> x;
	v = vector<int>(n);
	multiconjunts(0);
	return 0;
}
