#include <iostream>

#include <vector>
#include <string>

using namespace std;


int n, p;
vector<string> paraules;
vector<int> v; // indica a quin subconjunt va cada paraula



void escriu() {
	for (int s = 0; s < p; s++) {
		cout << "subconjunt " << s+1 << ": {";
		bool primera = true;
		for (int i = 0; i < n; i++) {
			if (v[i] == s) {
				if (primera) {
					cout << paraules[i];
					primera = false;
				}
				else cout << ',' << paraules[i];
			}
		}
		cout << '}' << endl;
	}
	cout << endl;
}


void f(int i) {
	if (i == n) escriu();
	else {
		for (int s = 0; s < p; s++) {
			v[i] = s;
			f(i+1);
		}
	}
}

int main() {
	cin >> n;
	paraules = vector<string>(n);
	for (int i = 0; i < n; i++) cin >> paraules[i];
	cin >> p;
	v = vector<int>(n);
	f(0);
	
	return 0;
}
	
