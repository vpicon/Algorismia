#include <iostream>
#include <vector>

using namespace std;


int main() {
	cout.setf(ios::fixed);
    cout.precision(4);
    int n; 
    while (cin >> n) {
	    float f;
	    float prob = 1;
	    while(n--) {
			cin >> f; 
			prob *= (1-f);  // prob que no falli 
	    }
	    cout << 1-prob << endl;
	}
}
    
