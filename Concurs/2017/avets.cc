#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

int n, k;
VI e;  // e[i] dona energia de avet iessim
VI sum;  // cumsum of e
VVI xutes;  // matriu de programacio dinamica

// returns sum of energies of e between l <= r (r included)
int sum_lr(int l, int r) {
	if (l > r or l < 0 or r >= n) return 0;
	if (l == 0) return sum[r];
	return sum[r] - sum[l-1];
}
	

// Retorna el minim nombre de xutes que cal per a emvolicar avets de e entre l i r (0 <= l,r < n)
int min_xutes(int l, int r) {
	if (l >= r or l < 0 or r >= n) return 0;
	
	int& ans = xutes[l][r];
	if (ans != -1) return ans;
	
	if (sum_lr(l, r) <= k) return ans = 0; // ok case, no calen xutes
	int minim = n; // init case absurd (infinite)
	int m = l+1;
	for (int m = l+1; m < r; m++) {
		minim = min(minim, 1 + min_xutes(l, m) + min_xutes(m+1, r));
	}
	return ans = minim;
}


int main() {
	while (cin >> n >> k) {
		e = VI(n);
		sum = VI(n);
		xutes = VVI(n, VI(n, -1));
		
		for (int i = 0; i < n; i++) cin >> e[i];
		
		sum[0] = e[0];
		for (int i = 1; i < n; i++) sum[i] = e[i] + sum[i-1];
		
		cout << min_xutes(0, n-1) << endl;
	}
}


