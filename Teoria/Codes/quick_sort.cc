#include <iostream>
#include <vector>

using namespace std;

// sets the leftmost element as a partition of v (in order).
int partition(vector<int>& v, int l, int r) {
	int p = v[l];
	int i = l - 1;
	int j = r + 1;
	while (true) {
		while (v[++i] < p);
		while (v[--j] > p);
		if (i >= j) return j;
		swap(v[i], v[j]);
	}
}


// sorts vector v using quicksort algorithm
void quick_sort(vector<int>& v, int l, int r) {
	if (l <= r) {
		int p = partition(v, l, r);  // returns new position of v[0] and orders v as x.
		quick_sort(v, l, p);
		quick_sort(v, p + 1, r);	
		return;
	}
}	

	
		
// program to test selection sort algorithm
int main() {
	vector<int> v;
	int n;
	while (cin >> n) {
		v = vector<int> (n);  // init vector
		for (int i = 0; i < n; i++) cin >> v[i];  // read vector;
		quick_sort(v, 0, (int)v.size() - 1);  // sort
		for (int i = 0; i < n; i++) cout << v[i] << ' ';
		cout << endl;
	}
	return 0;
}
		
