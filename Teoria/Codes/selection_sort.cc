#include <iostream>
#include <vector>

using namespace std;

// sorts vector v using selection sort algorithm
void selection_sort(vector<int>& v) {
	int n = (int)v.size();
	for (int i = 0; i < n; i++) {
		int min = v[i];
		int jmin = i;
		for (int j = i; j < n; j++) {
			if (v[j] < min) {jmin = j; min = v[j];}
		}
		swap(v[i], v[jmin]);
	}
	return;
}
			
// program to test selection sort algorithm
int main() {
	vector<int> v;
	int n;
	while (cin >> n) {
		v = vector<int> (n);  // init vector
		for (int i = 0; i < n; i++) cin >> v[i];  // read vector;
		selection_sort(v);  // sort
		for (int i = 0; i < n; i++) cout << v[i] << ' ';
		cout << endl;
	}
	return 0;
}
		
