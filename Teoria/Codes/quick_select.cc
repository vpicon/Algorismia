#include <iostream>
#include <vector>

using namespace std;

// sets the leftmost element as a partition of v (in order).
int partition(vector<int>& v, int l, int r) {
	int p = v[l];
	int i = l - 1;
	int j = r + 1;
	while (true) {
		while (v[++i] < p);
		while (v[--j] > p);
		if (i >= j) return j;
		swap(v[i], v[j]);
	}
}


// returns the iths smallest element in v
int quick_select(vector<int>& v, int l, int r, int i) {  // 1 <= i <= r - l + 1
	if (l >= r) return v[l];
	int p = partition(v, l, r);
	if (i < p - l + 1) return quick_select(v, l, p, i);
	else if (i > p - l + 1) return quick_select(v, p+1, r, i - (p-l+1));
	else return v[p];
}

		
// program to test selection sort algorithm
int main() {
	vector<int> v;
	int n, k;
	while (cin >> n >> k) {
		v = vector<int> (n);  // init vector
		for (int i = 0; i < n; i++) cin >> v[i];  // read vector;
		int element = quick_select(v, 0, v.size() - 1, k);
		cout << element << endl;
	}
	return 0;
}
		
