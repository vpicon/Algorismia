#include <iostream>
#include <string>
#include <vector>

using namespace std;

long eleva_potencia_modular(long b, long e, long n) { // Pre: b > 0, e >= 0, n > 0
  if (e == 0) return 1%n;
  long x = eleva_potencia_modular(b, e/2, n);
  if (e%2 == 0) return (x*x)%n;
  else return (((x*x)%n)*b)%n;
}

int main() {

  long N; cin >> N;
  long D; cin >> D;

  long clau_simetrica_enc; cin >> clau_simetrica_enc;
  long clau_simetrica = eleva_potencia_modular(clau_simetrica_enc, D, N);

  srand(clau_simetrica);

  long M;
  while (cin >> M) {
    long y = rand()%N;
    if (M - y < 0) cout << char(M - y + N);
    else cout << char(M - y);
  }
  cout << endl;

}

