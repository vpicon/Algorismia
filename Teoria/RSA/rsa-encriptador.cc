#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

long eleva_potencia_modular(long b, long e, long n) { // Pre: b > 0, e >= 0, n > 0
  if (e == 0) return 1%n;
  long x = eleva_potencia_modular(b, e/2, n);
  if (e%2 == 0) return (x*x)%n;
  else return (((x*x)%n)*b)%n;
}

int main() {

  long N; cin >> N;
  long E; cin >> E;

  srand(time(NULL));
  long clau_simetrica = rand()%N;
  long clau_simetrica_enc = eleva_potencia_modular(clau_simetrica, E, N);
  cout << clau_simetrica_enc << " ";

  srand(clau_simetrica);

  char c;
  while (cin >> c) {
    long M = long(c);
    long y = rand()%N;
    cout << (M + y)%N << " ";
  }
  cout << endl;

}

