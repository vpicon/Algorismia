#include <iostream>
#include <string>
#include <vector>

using namespace std;

long eleva_potencia(long b, long e) { // Pre: b > 0 i e >= 0
  if (e == 0) return 1;
  long x = eleva_potencia(b, e/2);
  if (e%2 == 0) return x*x;
  else return x*x*b;
}


bool es_primer(long x) { // algorisme lent
  if (x == 0 or x == 1) return false;
  if (x == 2) return true;
  if (x%2 == 0) return false;
  for (int i = 3; i*i <= x; i += 2) {
    if (x%i == 0) return false;
  }
  return true;
}

long genera_nombre(int n) { // genera un nombre de n bits a l'atzar
  long LIM = eleva_potencia(2, n+1);
  long x = LIM/2 + rand()%(LIM/2);
  return x;
}

long genera_primer(int n) { // genera un primer de n bits a l'atzar
  long x = genera_nombre(n);
  while (not es_primer(x)) x = genera_nombre(n);
  return x;
}

long mcd(long a, long b) { // Pre: a >= 0 i b >= 0
  if (b == 0) return a;
  return mcd(b, a%b);
}

long mcd_extes(long a, long b, long& x, long& y) { // retorna mcd(a, b) i x i y t.q. ax + by = mcd(a, b)

  if (b == 0) { x = 1; y = 0; return a; }
  long xp, yp;
  long d = mcd_extes(b, a%b, xp, yp);
  x = yp; y = xp - (a/b)*yp;
  return d;
}

long invers(long a, long b) { // Pre: mcd(a,b) == 1
  long x, y;
  long d = mcd_extes(a, b, x, y);
  if (d != 1) return -1;
  if (x > 0) return x;
  else return (x%b) + b;
}
  
  

int main() {

  int llavor; cin >> llavor;
  int n; cin >> n;

  srand(llavor);

  long P = genera_primer(n/2);
  long Q = genera_primer(n/2);
  while (Q == P) Q = genera_primer(n/2);
  long N = P*Q;
  long PHI = (P-1)*(Q-1);
  long E = 2;
  while (mcd(E, PHI) != 1) E = genera_nombre(n);
  long D = invers(E, PHI);

  cout << endl;
  cout << "llavor del generador: " << llavor << endl;
  cout << "bits de seguretat: n = " << n << endl;
  cout << "P = " << P << endl;
  cout << "Q = " << Q << endl;
  cout << "PHI = " << PHI << endl;
  cout << "E = " << E << endl;
  cout << "D = " << D << endl;
  cout << "comprovacio: E*D = " << E*D;
  cout << " i E*D mod PHI = " << (E*D)%PHI << endl;
  cout << endl;  
  cout << "clau publica: " << N << " " << E << endl;
  cout << "clau privada: " << N << " " << D << endl;
  cout << endl;

}
