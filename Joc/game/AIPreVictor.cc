#include "Player.hh"
#include <queue>

#define PLAYER_NAME PreVictor


struct PLAYER_NAME : public Player {
	
	static Player* factory () {  /** Factory: returns a new instance of this class. Do not modify this function. */
		return new PLAYER_NAME;
	}
	
	/* ------------ Main definitions and variables ------------- */
	
	typedef vector<int> VI;
	typedef vector<VI> VVI;
	typedef vector<bool> VB;
	typedef vector<VB> VVB;
	
	typedef vector<Pos> VPos;
	typedef vector<VPos> VVPos;
	typedef pair<Pos, bool> PosB;
	typedef vector<pair<Pos, bool>> VPosB;
	
	typedef set<int> S;  
    
    
    void write_pos(Pos p) {  // debug function
		cerr << '(' << p.i << ',' << p.j << ')';
	}
    
    
    const int INF = 1e9;
    VPos T;  // vector of all treasures in the map
    VVI Mdist;  // distances to treasures;
    
    
    
    /* --------------- Routines ------------------- */
    
    /* Initialization function, called at the very first round. */
    void __init__() {  
		
		// Generate vector of treasures T
	    for (int i = 2; i < 58; i++) {  // Check moria map
			for (int j = 2; j < 58; j++) {
				if(cell(i, j).treasure) T.push_back(Pos(i,j));
			}
		}
		Mdist = bfs_treasure_dist();  // Generates first distance matrix for treasures
	}
	
	
	/* Returns a matrix of minimum distances to each treasures; */
	VVI bfs_treasure_dist() {
		VVI min_dist(60, VI(60, INF));  // Matrix of distances inside Moria Map (Moria Coordinate system)
		for (Pos p_tr : T) {
			if (cell(p_tr).treasure) {  // There is still a treasure
				queue<Pos> pending;  // Pending vertices to visit in bfs
				VVI dist(60, VI(60, INF));  // Matrix of distance to that treasure
				
				dist[p_tr.i][p_tr.j] = 0;
				min_dist[p_tr.i][p_tr.j] = 0;
				pending.push(p_tr);
				
				while (not pending.empty()) {
					Pos p = pending.front(); pending.pop();
					
					for (int k = 0; k < 8; k++) {  // Try all possible movements
						Pos new_p = p + Dir(k);
						if (pos_ok(new_p) and dist[new_p.i][new_p.j] == INF  // Check potential new cell
						and cell(new_p).type != 2 and cell(new_p).type != 3) { // Check no granit nor abyss type
							dist[new_p.i][new_p.j] = dist[p.i][p.j] + 1;
							if (cell(new_p).type == Rock) dist[new_p.i][new_p.j] += cell(new_p).turns;
							if (dist[new_p.i][new_p.j] < min_dist[new_p.i][new_p.j]) min_dist[new_p.i][new_p.j] = dist[new_p.i][new_p.j];
							pending.push(new_p);
						}
					}
				}
				
			}
		}
		return min_dist;
	}		
	
	
	/* Given a matrix of minimum distance Mdist, and a position p
	 * returns the adjacent position to p with minimum distance in Mdist. */
	int min_dist_move (const VVI& Mdist, Pos p, bool diag) {  // diag true if diagonal moves are allowed
		int opt_dir = random(0,7);  // stay still if no optimum achieved (absurd)
		int d_min = INF;
			
		for (int i = 0; i < 8; i += 1) {
			Pos next_p = p + Dir(i);
			if (pos_ok(next_p) and Mdist[next_p.i][next_p.j] < d_min) {
				opt_dir = i;
				d_min = Mdist[next_p.i][next_p.j];
			}
			if (not diag) i++;  // skip odd i -> diagonals
		}
		return opt_dir;
	}
	
	
	
	bool own_teammate(Pos p) {
		int id = cell(p).id;
		if (id != -1 and unit(id).player == me()) return true;
		return false;
	}
	
	
		
	/* Moves dwarves to nearest treasure according to the Mdist matrix. */
	void move_dwarves_to_treasure(VI Dw, const VVI& Mdist) {		

		for (int id : Dw) {
			Unit dw = unit(id);  // Current dwarf 
			int i_next_dir = min_dist_move(Mdist, dw.pos, true);  // Position of dwarf dw
			command(id, Dir(i_next_dir));
		}	
    }
    
    
    /* Sends direction for id to go to nearest Outside cell.*/
    int go_outside(int id) {  // via bfs
		if (cell(unit(id).pos).type == Outside) return 8;  // stay still
		else {
			
		VVI dist(60, VI(60, INF));
		queue<Pos> pending;
		bool diag = (unit(id).type == Dwarf);  
		
		Pos p0 = unit(id).pos;
		pending.push(p0);
		dist[p0.i][p0.j] = 0;
		
		while (not pending.empty()) {
			Pos p = pending.front(); pending.pop();
			
			if (cell(p).type == Outside) {
				bool diag = (unit(id).type == Dwarf);
				
				int dir = 8;  // Init as no move
				while (p != p0) {
					dir = min_dist_move(dist, p, diag);
					p += Dir(dir);
				}
				return (dir + 4)%8;
			}	
			for (int m = 0; m < 8; m++) {
				Pos next_p = p + Dir(m);
				if (pos_ok(next_p) and dist[next_p.i][next_p.j] == INF 
				and (cell(next_p).type == Cave  or cell(next_p).type == Outside)
				and cell(next_p).id == -1) {
					dist[next_p.i][next_p.j] = dist[p.i][p.j] + 1;
					pending.push(next_p);
				}
				if (not diag) m++;  // skip diagonal moves for wizards
			}
		}
		return random(0, 7);  // move randomly in the critical case
	}
	
	}
    
    
    // Returns if the cell at p is completely free.
    bool free_cell(Pos p) {
		return (cell(p).id == -1 and cell(p).type <= 1);
	}
		
    /*
    // Gives the single simple direction (possible) of movement from p to q
    // Works great for sufficiently near p and q. Otherwise seek for a shortest path
    int directed_possible_movement(Pos p, Pos q, bool fast) {  // fast bool for a fast move (will move only to Cave type)
		int diescape_surrounding(r = random(0,7);  // init as random;
		int Di = q.i - p.i;
		int Dj = q.j - p.j;
		if (Di > 0) {
			if (Dj > 0 and free_cell(p + BR)) return BR;
			if (Dj == 0 and free_cell(p + Bottom)) return Bottom;
			if (Dj < 0 and free_cell(p + BL)) return BL;
		
		
	}
	*/
    
    
    
    int dir_shortest_path(Pos p_s, Pos p_f, bool diag) {
		VVI dist(60, VI(60, INF)); // CAMBIAR PARA HACER MATRIZ DE TAMAÑO DADO (eg 7x7) CENTRADA EN p
		queue<Pos> pending;
		
		pending.push(p_s);
		dist[p_s.i][p_s.j] = 0;
		
		while (not pending.empty()) {
			Pos p = pending.front(); pending.pop();
			
			if (p == p_f) {
				int dir = random(0,7);  // Init as random
				while (p != p_s) {
					dir = min_dist_move(dist, p, diag);
					p += Dir(dir);
				}
				return (dir + 4)%8;
			}
			
			for (int m = 0; m < 8; m++) {
				Pos next_p = p + Dir(m);
				if (pos_ok(next_p) and dist[next_p.i][next_p.j] == INF 
				and (cell(next_p).type == Cave  or cell(next_p).type == Outside)
				and cell(next_p).id == -1) {
					dist[next_p.i][next_p.j] = dist[p.i][p.j] + 1;
					pending.push(next_p);
				}
				if (not diag) m++;  // skip diagonal moves for wizards
			}
		}
		return random(0, 7);  // return random in critical case (error)
	}
			
			
		
		
		
    
    
    /* Escapes from enemies: Balrog and Trolls and Orcs */
    void escape_Balrog (const VI& Vid) {
		// Watch Linf Ball of radius 6 adjacent blocks from Balrog
		Pos p_bal = unit(balrog_id()).pos;

		for (auto id : Vid) {  // costless than a 10*10 square
			Pos p = unit(id).pos;
			int di = p_bal.i - p.i; 
			int dj = p_bal.j - p.j;
			if (max(abs(di), abs(dj)) <= 5) {
				int dir = go_outside(id);
				command(id, Dir(dir));
				//Sid.erase(id);
			}
		}
		//return not_escaped;
	}
    
    /*
	void attack_escape_surrounding(int id) {
		// search around a 5*5 square
		priority_queue<pair<int, Pos>> Q;
		for (int i = -2; i <= 2; i++) { 
			for (int j = -2; j <= 2; j++) {
				Pos p = unit(id).pos + Pos(i, j);
				int id = cell(p).id;
				int type;
				if (id != -1) {
					type = unit(id).type;
					if (type == Troll) Q.push( pair<int,Pos> (1, p) );
					if (type == Orc) Q.push( pair<int,Pos> (2, p) );
					if (type == Dwarf or type == Wizard) 
						if (cell(p).owner != me()) Q.push( pair<int,Pos> (3, p) );
				}
			}
		}
		
		if (not Q.empty()) {
			Pos p = Q.top().second;
			int kind = Q.top().first;  // kind of enemy
			Pos p0 = unit(id).pos;
			int dir;
			if (kind == 1) dir = go_outside(id); //directed_possible_movement(p, p0);
			else dir = dir_shortest_path(p0, p, (bool)(unit(id).type == Dwarf));
			command(id, Dir(dir));
		}
		return;
	}
	*/
	
	void attack_adjacent(int id) {
		for (int m = 0; m < 8; m++) {
			Pos p = unit(id).pos + Dir(m);
			if (cell(p).id != -1 and cell(p).owner != me()) {
				command(id, Dir(m));
			}
		}
	}
			
		
    
    
    
    /* ------------ Play and Stuff ------------- */
    
	virtual void play () { /** Play method, invoked once per each round. */
		if (round() == 0) __init__();
		
		VI Dw = dwarves(me()); // vector of IDs of own dwarves
		//S Sid = S(Dw.begin(), Dw.end());
		
		escape_Balrog(Dw); // escapes own mobs and return those that did not execute command to escape
		for (int id : Dw) attack_adjacent(id);
		if (round() % 2 == 0) Mdist = bfs_treasure_dist();
		move_dwarves_to_treasure(Dw, Mdist);
				
	}

};


/** Do not modify the following line. */
RegisterPlayer(PLAYER_NAME);
