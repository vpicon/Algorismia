#include "Player.hh"

#define PLAYER_NAME Victor


struct PLAYER_NAME : public Player {
	/**  
	* Factory: returns a new instance of this class.
	* Do not modify this function.
	*/
	static Player* factory () {
		return new PLAYER_NAME;
	}
    /**
     * Types and attributes for your player can be defined here.
     */
   
	/*  ------------ Main definitions and variables ----------------  */
	
	typedef vector<int> VI;
	typedef map<int, int> MI;
	typedef stack<int> S;
	
	struct group {
		int group_id;  // id of the group (integer between 0 and number_of_groups-1)
		S members;  // stack of all ids 
		int role;  // 0 if treasurer, 1 if cell defender/conquerer, 2 if attacker
		Pos group_pos;  // position of the group;
		int leader_id;
	}
		
	typedef map<int, group> GMap;  // map of groups identified by id of group
	GMap GROUPS;
	
	
    
    
    /* ------------- Initialization methods ------------------------- */
    void init_groups(S& GROUPS) {
		int n_groups = 7;  // starting number of groups
		if 
		
		
		
    
    
    void init () {
		init_groups(GROUPS);
		
	}
    
    
    
    
    /* -------------- Main Functions and subroutines ---------------- */
    
	
	void move () {
		return;
	}
	
	
	
	
	
	/* -------------------- Play method and stuff ------------------- */
	
	virtual void play () {  /** Play method, invoked once per round. */
		if (round() == 0) init();
		move();
	}	

};



/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
