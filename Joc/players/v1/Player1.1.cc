#include "Player.hh"


#define PLAYER_NAME Victor


struct PLAYER_NAME : public Player {
	/**  
	* Factory: returns a new instance of this class.
	* Do not modify this function.
	*/
	static Player* factory () {
		return new PLAYER_NAME;
	}
    /**
     * Types and attributes for your player can be defined here.
     */
   
	/*  ------------ Main definitions and variables ----------------  */
	
	typedef vector<int> VI;
	typedef vector<VI> VVI;
	
	typedef vector<Pos> VP;
	
	typedef map<int, int> MI;
	typedef stack<int> S;
	
	
	
	typedef struct group {
		int group_id;  // id of the group (integer between 0 and number_of_groups-1)
		S members;  // stack of all ids 
		int role;  // 0 if treasurer, 1 if cell defender/conquerer, 2 if attacker
		Pos group_pos;  // position of the group;
		int leader_id;
		
		group(int id) {  // Group constructor
			group_id = id;
		}
		
	} group;
		
	typedef vector<group> VG;  // map of groups identified by id of group
	VG GROUP;
	
    
    
    /* ------------- Initialization methods ------------------------- */
    
		
		
    void init_groups(VG& GROUP) {
		VI D = dwarves(me());
		VI W = wizards(me());
		
		int n_groups = 8;  // starting number of groups
		VP initial_pos {  // 8 initial positions
			Pos(29-8, 8), Pos(8, 29-8), Pos(8, 30+8), Pos(29-8, 59-8), Pos(30+8, 59-8), Pos(59-8, 30+8), Pos(59-8, 29-8), Pos(30+8, 8) 
		};
		
		for (int g = 0; g < 8; g++) {
			group G = group(g);
			G.group_pos = initial_pos[g];  // Fer millor per OCTANTS ??
			GROUP.push_back(G);
		}   
		
		for (int d : D) {
			int g = octant(unit(d).pos);
			(GROUP[g].members).push(d);
		}
		for (int w : W) {
			int g = octant(unit(w).pos);
			(GROUP[g].members).push(w);
			cerr << GROUP[g].members.size() << endl;
		}
		
	}
	
	
			
			
		
		
		
    
    
    void init () {
		init_groups(GROUP);
		
	}
    
    
    
    
    /* -------------- Main Functions and subroutines ---------------- */
    
    /* POSITIONING METHODS */
    
	int octant(Pos p) {  // Position at the board, returns number of octant of position p  (with circuilar definition of octants)
		if (p.i <= 29) {  // upper half
			if (p.j <= 29) {  // left half
				if (p.i >= p.j) return 0;
				else return 1;
			}
			else {  // right half
				if (p.i + p.j <= 59) return 2;
				else return 3;
			}
		}
		else {  // lower half
			if (p.j >= 30) {
				if (p.i < p.j) return 4;
				else return 5;
			}
			
			else {
				if (p.i + p.j > 59) return 6;
				else return 7;
			}
		}
	}
	
	
	/* MOVEMENT METHODS */
	
	
	void move () {

	}
	
	
	
	
	
	/* -------------------- Play method and stuff ------------------- */
	
	virtual void play () {  /** Play method, invoked once per round. */
		if (round() == 0) init();
		/*if (round() == 199) {
			VVI OCT (60, VI(60));
			
			VP initial_pos {  // 8 initial positions
				Pos(29-8, 8), Pos(8, 29-8), Pos(8, 30+8), Pos(29-8, 59-8), Pos(30+8, 59-8), Pos(59-8, 30+8), Pos(59-8, 29-8), Pos(30+8, 8) 
			};
			
			for (int i = 0; i < 60; i++) {
				for (int j = 0; j < 60; j++) {
					OCT[i][j] = octant(Pos(i,j));
				}
			}
			for (Pos p : initial_pos) {
				OCT[p.i][p.j] = 8;
			}
			for (int i = 0; i < 60; i++) {
				for (int j = 0; j < 60; j++) {
					cerr << OCT[i][j] << ' ';
				}
				cerr << endl;
			}
		}*/
		/*
		for (auto id : GROUP.at(0).members) {
			command(id, Right);
		}*/
		init_groups(GROUP);
		cerr << GROUP.size() << endl;
	}	
	
};



/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
