#include "Player.hh"
#include <queue>

#define PLAYER_NAME Victor


struct PLAYER_NAME : public Player {
	
	static Player* factory () {  /** Factory: returns a new instance of this class. Do not modify this function. */
		return new PLAYER_NAME;
	}
	
	/* ------------ Main definitions and variables ------------- */
	
	typedef vector<int> VI;
	typedef vector<VI> VVI;
	typedef vector<bool> VB;
	typedef vector<VB> VVB;
	
	typedef vector<Pos> VPos;
	typedef pair<Pos, bool> PosB;
	typedef vector<pair<Pos, bool>> VPosB;
    
    
    void write_pos(Pos p) {  // debug function
		cerr << '(' << p.i << ',' << p.j << ')';
	}
    
    
    const int INF = 1e9;
    VPos T;  // vector of all treasures in the map
    VVI Mdist;  // distances to treasures;
    
    
    
    /* --------------- Routines ------------------- */
    
    /* Initialization function, called at the very first round. */
    void __init__() {  
		
		// Generate vector of treasures T
	    for (int i = 2; i < 58; i++) {  // Check moria map
			for (int j = 2; j < 58; j++) {
				if(cell(i, j).treasure) T.push_back(Pos(i,j));
			}
		}
		
		Mdist = bfs_treasure_dist();  // Generates first distance matrix for treasures
	}
	
	
	/* Returns a matrix of minimum distances to each treasures; */
	VVI bfs_treasure_dist() {
		VVI min_dist(60, VI(60, INF));  // Matrix of distances inside Moria Map (Moria Coordinate system)
		for (Pos p_tr : T) {
			if (cell(p_tr).treasure) {  // There is still a treasure
				queue<Pos> pending;  // Pending vertices to visit in bfs
				VVI dist(60, VI(60, INF));  // Matrix of distance to that treasure
				
				dist[p_tr.i][p_tr.j] = 0;
				min_dist[p_tr.i][p_tr.j] = 0;
				pending.push(p_tr);
				
				while (not pending.empty()) {
					Pos p = pending.front(); pending.pop();
					
					for (int k = 0; k < 8; k++) {  // Try all possible movements
						Pos new_p = p + Dir(k);
						if (pos_ok(new_p) and dist[new_p.i][new_p.j] == INF  // Check potential new cell
						and cell(new_p).type != 2 and cell(new_p).type != 3) { // Check no granit nor abyss type
							dist[new_p.i][new_p.j] = dist[p.i][p.j] + 1;
							if (dist[new_p.i][new_p.j] < min_dist[new_p.i][new_p.j]) min_dist[new_p.i][new_p.j] = dist[new_p.i][new_p.j];
							pending.push(new_p);
						}
					}
				}
				
			}
		}
		return min_dist;
	}		
	
	
	/* Given a matrix of minimum distance Mdist, and a position p
	 * returns the adjacent position to p with minimum distance in Mdist. */
	int min_dist_move (const VVI& Mdist, Pos p, bool diag) {  // diag true if diagonal moves are allowed
		int opt_dir = random(0,7);  // stay still if no optimum achieved (absurd)
		int d_min = INF;
			
		for (int i = 0; i < 7; i += 1) {
			Pos next_p = p + Dir(i);
			if (pos_ok(next_p) and Mdist[next_p.i][next_p.j] < d_min) {
				opt_dir = i;
				d_min = Mdist[next_p.i][next_p.j];
			}
			if (not diag) i++;  // skip odd i -> diagonals
		}
		return opt_dir;
	}
	
		
	/* Moves dwarves to nearest treasure according to the Mdist matrix. */
	void move_dwarves_to_treasure(VI Dw, const VVI& Mdist) {		

		for (int id : Dw) {
			Unit dw = unit(id);  // Current dwarf 
			int i_next_dir = min_dist_move(Mdist, dw.pos, true);  // Position of dwarf dw
			command(id, Dir(i_next_dir));
		}
			
    }
    
    
    /* Sends direction for id to go to nearest Outside cell.*/
    int go_outside(int id) {  // via bfs
		if (cell(unit(id).pos).type == Outside) return 8;  // stay still
		else {
			
		VVI dist(60, VI(60, INF));
		queue<Pos> pending;
		bool diag = (unit(id).type == Dwarf);  
		
		Pos p0 = unit(id).pos;
		pending.push(p0);
		dist[p0.i][p0.j] = 0;
		
		while (not pending.empty()) {
			Pos p = pending.front(); pending.pop();
			
			if (cell(p).type == Outside) {
				bool diag = (unit(id).type == Dwarf);
				write_pos(p0); cerr << endl;
				int dir = 8;  // Init as no move
				while (p != p0) {
					cerr << "here" << endl;
					dir = min_dist_move(dist, p, diag);
					p += Dir(dir);
				}
				return (dir + 4)%8;
			}	
			for (int m = 0; m < 7; m++) {
				Pos next_p = p + Dir(m);
				if (pos_ok(next_p) and dist[next_p.i][next_p.j] == INF 
				and (cell(next_p).type == Cave  or cell(next_p).type == Outside)
				and cell(next_p).id == -1) {
					dist[next_p.i][next_p.j] = dist[p.i][p.j] + 1;
					pending.push(next_p);
				}
				if (not diag) m++;  // skip diagonal moves for wizards
			}
		}
		return random(0, 7);  // move randomly in the critical case
	}
	
	}
    
    
    /* Escapes from enemies: Balrog and Trolls and Orcs */
    void escape_Balrog (const VI& Vid) {
		// Watch Linf Ball of radius 6 adjacent blocks from Balrog
		Pos p_bal = unit(balrog_id()).pos;

		for (int id : Vid) {
			Pos p = unit(id).pos;
			int di = p_bal.i - p.i; 
			int dj = p_bal.j - p.j;
			if (max(abs(di), abs(dj)) <= 6) {
				int dir = go_outside(id);
				command(id, Dir(dir));
			}
		}
		//return not_escaped;
	}
    
    
    
    
    /* ------------ Play and Stuff ------------- */
    
	virtual void play () { /** Play method, invoked once per each round. */
		if (round() == 0) __init__();
		
		VI Dw = dwarves(me()); // vector of IDs of own dwarves
		
		if (round() > 180) {  // DEBUG
			int id = Dw[0];
			int dir = go_outside(id);
			write_pos(unit(id).pos); //cerr << ' ' << dir << endl;
			command(id, Dir(dir));
		}
		

		//escape_Balrog(Dw); // escapes own mobs and return those that did not execute command to escape
		
		if (round() % 3 == 0) Mdist = bfs_treasure_dist();
		move_dwarves_to_treasure(Dw, Mdist);
				
	}

};


/** Do not modify the following line. */
RegisterPlayer(PLAYER_NAME);
