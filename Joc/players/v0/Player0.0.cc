#include "Player.hh"

#define PLAYER_NAME Victor


struct PLAYER_NAME : public Player {
	/**  
	* Factory: returns a new instance of this class.
	* Do not modify this function.
	*/
	static Player* factory () {
		return new PLAYER_NAME;
	}
    /**
     * Types and attributes for your player can be defined here.
     */
   
	/*  ------------ Main definitions and variables ----------------  */
	
	typedef vector<int> VI;
	
	
	
	
    
    
    /* -------------- Main Functions and subroutines ---------------- */
    void init () {
		return;
	}
	
	void move () {
		return;
	}
	
	
	
	
	
	/* -------------------- Play method and stuff ------------------- */
	
	virtual void play () {  /** Play method, invoked once per round. */
		if (round() == 0) init();
		move();
	}	

};



/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);
