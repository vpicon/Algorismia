#include "Player.hh"
#include <queue>

#define PLAYER_NAME Victor


struct PLAYER_NAME : public Player {
	
	static Player* factory () {  /** Factory: returns a new instance of this class. Do not modify this function. */
		return new PLAYER_NAME;
	}
	
	/* ---- Main definitions and variables ---- */
	
	typedef vector<int> VI;
	typedef vector<VI> VVI;
	typedef vector<bool> VB;
	typedef vector<VB> VVB;
	
	typedef vector<Pos> VPos;
	typedef pair<Pos, bool> PosB;
	typedef vector<pair<Pos, bool>> VPosB;
    
    
    
    const int INF = 1e9;
    VPos T;  // vector of all treasures in the map
    VVI Mdist;  // distances to treasures;
    
    
    
    /* -------- Routines -------- */
    
    /* Initialization function, called at the very first round. */
    void __init__() {  
		
		// Generate vector of treasures T
	    for (int i = 2; i < 58; i++) {  // Check moria map
			for (int j = 2; j < 58; j++) {
				if(cell(i, j).treasure) T.push_back(Pos(i,j));
			}
		}
		
	}
	
	
	/* Returns a matrix of minimum distances to each treasures; */
	VVI bfs_treasure_dist() {
		VVI min_dist(60, VI(60, INF));  // Matrix of distances inside Moria Map (Moria Coordinate system)
		for (Pos p_tr : T) {
			if (cell(p_tr).treasure) {  // There is still a treasure
				queue<Pos> pending;  // Pending vertices to visit in bfs
				VVI dist(60, VI(60, INF));  // Matrix of distance to that treasure
				
				dist[p_tr.i][p_tr.j] = 0;
				min_dist[p_tr.i][p_tr.j] = 0;
				pending.push(p_tr);
				
				while (not pending.empty()) {
					Pos p = pending.front(); pending.pop();
					
					for (int k = 0; k < 8; k++) {  // Try all possible movements
						Pos new_p = p + Dir(k);
						if (pos_ok(new_p) and dist[new_p.i][new_p.j] == INF  // Check potential new cell
						and cell(new_p).type != 2 and cell(new_p).type != 3) { // Check no granit nor abyss type
							dist[new_p.i][new_p.j] = dist[p.i][p.j] + 1;
							if (dist[new_p.i][new_p.j] < min_dist[new_p.i][new_p.j]) min_dist[new_p.i][new_p.j] = dist[new_p.i][new_p.j];
							pending.push(new_p);
						}
					}
				}
				
			}
		}
		return min_dist;
	}		
	
	
	/* Given a matrix of minimum distance Mdist, and a position p
	 * returns the adjacent position to p with minimum distance in Mdist */
	int min_dist_move (const VVI& Mdist, Pos p, bool diag) {  // diag true if diagonal moves are allowed
		int opt_dir = 8;  // stay still if no optimum achieved (absurd)
		int d_min = INF;
			
		for (int i = 0; i < 7; i += 1) {
			Pos next_p = p + Dir(i);
			if (pos_ok(next_p) and Mdist[next_p.i][next_p.j] < d_min) {
				opt_dir = i;
				d_min = Mdist[next_p.i][next_p.j];
			}
			if (not diag) i++;  // skip odd i -> diagonals
		}
		return opt_dir;
	}
	
		
	/* Moves dwarves to nearest treasure according to the Mdist matrix */
	void move_dwarves_to_treasure(const VVI& Mdist) {
		VI Dw = dwarves(me()); // vector of IDs of own dwarves 
		int Ndw = Dw.size(); // Number of dwarves		

		for (int id_dw : Dw) {
			Unit dw = unit(id_dw);  // Current dwarf 
			int i_next_dir = min_dist_move(Mdist, dw.pos, true);  // Position of dwarf dw
			command(id_dw, Dir(i_next_dir));
		}
			
    }
  
	
	virtual void play () { /** Play method, invoked once per each round. */
		if (round() == 0) {
			__init__();
			Mdist = bfs_treasure_dist();  // SHOULD be inside init.
		}
		move_dwarves_to_treasure(Mdist);
		if (round() % 3 == 0) Mdist = bfs_treasure_dist();
		
		if (round() == 198) {
			VVI Mdist = bfs_treasure_dist();
			for (int i = 0; i < (int)Mdist.size(); i++) {
				for (int j = 0; j < (int)Mdist[0].size(); j++) cerr << Mdist[i][j] << ' ';
				cerr << endl;
			}
		}
		
	}

};


/** Do not modify the following line. */
RegisterPlayer(PLAYER_NAME);
