#include <vector>
#include <iostream>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;
using VB = vector<bool>;

int g, n;
int iaies;
VI v;  // v[j] diu quantes iaies hi ha en la escola j (0 <= j < n)
VI b;  // b[k] diu on esta el grup de bombers k. (0 <= k < g)
VB p;  // v[j] diu si la escola j esta totalment protegida per algun grup
	
	
// Calcula el nombre maxim de iaies que salvem
void f(int k, int x) {  // anem pel grup de bombers k, // portem x iaies salvades
	if (k == g) iaies = max(iaies, x);
	int jmax = n - g + k; // jmax = (n-1) - (g-k-1); hi hem de encabir els bombers que faltin al final
	if (k == 0) {
		for (int j = 0; j <= jmax; j++) {
			b[k] = j;  // indiquem posicio del grup de bombers
			p[j] = true;  // indiquem escola j ocupada
			x += v[j]; // salva les iaies de escola j
			
			// salvem iaies de escoles adjacents, vigilant casos extrems
			if (j > 0) x += v[j-1]/2;
			if (j < n-1) x+= v[j+1]/2; 
			f(k + 1, x);
			
			// reconfigurem per a backtracking
			x -= v[j];
			if (j > 0) x -= v[j-1]/2;
			if (j < n-1) x-= v[j+1]/2; 
			p[j] = false;  
		}
	}
	else if (k < g) {
		int jmin = b[k-1] + 1;  // grup k comenca a la escola a la dreta del anterior
		for (int j = jmin; j <= jmax; j++) {
			b[k] = j;  // indiquem posicio del grup de bombers
			p[j] = true;  // indiquem escola j ocupada
			if (j == jmin) x -= v[j]/2; // descomptem actual de sumat anterior.
			
			x += v[j]; // salva les iaies de escola j
			// salvem iaies de escoles adjacents, vigilant casos extrems
			if (not p[j-1]) x += v[j-1]/2;
			if (j < n-1 and not p[j+1]) x+= v[j+1]/2; 
			f(k + 1, x);
			
			// reconfigurem per a backtracking
			x -= v[j];
			if (not p[j-1]) x -= v[j-1]/2;
			if (j < n-1 and not p[j+1]) x-= v[j+1]/2; 
			p[j] = false;  
			if (j == jmin) x += v[j]/2; // descomptem actual de sumat anterior.
		}
		
	}
	return;
}
	
	
int main() {
	while (cin >> g >> n) {
		iaies = 0;  // inicialitza maxim nombre a 0
		v = VI(n, 0);
		p = VB(n, false);
		b = VI(g, 0);
		for (int j = 0; j < n; j++) cin >> v[j];
		f(0, 0);  // init pel primer grup de bombers, 0 iaies salvades
		cout << iaies << endl;
	}
	return 0;
}
