#include <vector>
#include <iostream>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;



int n, r;
VVI M;  // posicions dels r reis: M[k] = [i,j].
VVI W;  // W[i][j] indica quants reis amenacen posicio (i,j).

void escriu_buit() {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) cout << '.';
		cout << endl;
	}
	cout << "----------" << endl;
	return;
}


// Diu si la posicio (i, j) cau dins de matriu n x n;
bool ok(int i, int j) {
	return (0 <= i and i < n) and (0 <= j and j < n);
}

void escriu() {
	int k = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (k < r and M[k][0] == i and M[k][1] == j) {
				cout << 'K';
				k++;
			}
			else cout << '.';
		}
		cout << endl;
	}
	cout << "----------" << endl;
	return;
}

void amenaza(int i, int j) {
	if (ok(i-1, j-1)) W[i-1][j-1] += 1;
	if (ok(i-1, j)) W[i-1][j] += 1;
	if (ok(i-1, j+1)) W[i-1][j+1] += 1;
	if (ok(i, j-1)) W[i][j-1] += 1;
	if (ok(i, j+1)) W[i][j+1] += 1;
	if (ok(i+1, j-1)) W[i+1][j-1] += 1;
	if (ok(i+1, j)) W[i+1][j] += 1;
	if (ok(i+1, j+1)) W[i+1][j+1] += 1;
	return;
}

void desamenaza(int i, int j) {
	if (ok(i-1, j-1)) W[i-1][j-1] -= 1;
	if (ok(i-1, j)) W[i-1][j] -= 1;
	if (ok(i-1, j+1)) W[i-1][j+1] -= 1;
	if (ok(i, j-1)) W[i][j-1] -= 1;
	if (ok(i, j+1)) W[i][j+1] -= 1;
	if (ok(i+1, j-1)) W[i+1][j-1] -= 1;
	if (ok(i+1, j)) W[i+1][j] -= 1;
	if (ok(i+1, j+1)) W[i+1][j+1] -= 1;
	return;
}


// Escriu 
void reis (int k) {
	if (k == r) escriu();
	if (k == 0) {
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++) {
				M[0][0] = i, M[0][1] = j;
				amenaza(i, j);  //amenaça posicions de ficar rei en i,j
				reis(1); // k = 1;
				desamenaza(i, j);  // treu el rei de i,j
			}
		}
	}
	else if (k < r) {
		int i_ant = M[k-1][0];
		int j_ant = M[k-1][1];
		for (int j = j_ant + 2; j < n; j++) {  // començem am i = i_ant, sabem que j >= jant+2
			if (W[i_ant][j] == 0) {
				amenaza(i_ant, j);
				M[k][0] = i_ant, M[k][1] = j;
				reis(k+1);
				desamenaza(i_ant, j);
			}
		}
		for (int i = i_ant + 1; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (W[i][j] == 0) {
				amenaza(i, j);
				M[k][0] = i, M[k][1] = j;
				reis(k+1);
				desamenaza(i, j);
				}
			}
		}
	}
	return;
}

int main() {
	cin >> n >> r;
	if (r == 0) escriu_buit();
	else {
		M = VVI(r, VI(2, 0));  // positions of the r kings
		W = VVI(n, VI(n, 0));

		reis(0);
	}
	return 0;
}
	
	
