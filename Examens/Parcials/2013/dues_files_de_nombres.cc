#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;


int n;
VI x, y;



void write(const VI& v) {
	cout << v[0];
	for (int i = 1; i < (int)v.size(); i++) cout << ' ' << v[i];
	cout << endl;
	return;
}

// f writes all forms of ordering x and y. we have read till 2i.
void f(int i) {
	return;
}


int main () {
	cin >> n;
	v = VI(2 * n);  // complete vector;
	for (int i = 0; i < 2*n; i++) cin >> v[i];
	sort(v.begin(), v.end()); 
	x = VI(n);
	y = VI(n);
	
	f(0);
	
	
	return 0;
}
