#include <iostream>
#include <vector>

using namespace std;
using VI = vector<int>;
using VB = vector<bool>;

VI v;  // vector amb la permutacio
VB usat;  // usat[i] diu si i ha sigut usat per la permutacio

int n;

void escriu() {
	cout << v[0];
	for (int i = 1; i < n; i++) cout << ' ' << v[i];
	cout << endl;
}

bool consec(int x, int y) {
	return (x - y == 1) or (y - x == 1);
}


// escriu totes les permutacions xules. Anem per la i.
void f(int i) {
	if (i == n) escriu();
	if (i == 0) {  // separem aquest cas, ja que no podem mirar xuleria
		for (int j = 0; j < n; j++) {  //recorrem tots els nombres de [n]
			v[i] = j;
			usat[j] = true;
			f(i + 1);
			usat[j] = false;
		}
	} 
	else if (i < n) {
		for (int j = 0; j < n; j++) {
			if (not usat[j]) {
				if (not consec(j, v[i-1])) {  // mirem xuleria
					v[i] = j;
					usat[j] = true;
					f(i + 1);
					usat[j] = false;
				}
			}
		}
	}
	return;
}
	


int main () {
	while (cin >> n) {
		v = VI(n);
		usat = VB(n, false);
		f(0);
		cout << "********************" << endl;
	}
	return 0;
}
