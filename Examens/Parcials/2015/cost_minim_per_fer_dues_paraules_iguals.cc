#include <iostream>
#include <vector>
#include <string>

using namespace std;
using VI = vector<int>;
using VVI = vector<VI>;

string s, t;
int n1, n2;
VVI C; // matriu de memoria, conté costos.

// Retorna cost minim de haver fet iguals les paraules des del final fins i, j
int f(int i, int j) { // digits que queden per comparar de s, t (no indexs dels vectors)
	if (i == 0 and j == 0) return 0; // no queden digits a comparar, cost nul
	int& res = C[i][j];
	if (res != -1) return res;
	int minim = 6000 + 1; // +inf: 1000 lletres comm a max, pitjor cas: esborrar tot
	if (i == 0) {  // no queden lletres de s
		if (s[i-1+1] == t[j-1]) minim = min(minim, f(i, j-1) + 2);  // dupliquem una lletra de s, cost 2 
		minim = min(minim, f(i, j-1) + 3); // borrem lletra de t
	}
	else if (j == 0) { // no queden lletres de t
		if (s[i-1] == t[(j-1)+1]) minim = min(minim, f(i-1, j) + 2); // dupliquem una lletra de t, cost 2 
		minim = min(minim, f(i-1, j) + 3); // borrem lletra de s
	}
	else {
		if (s[i-1] == t[j-1]) minim = min(minim, f(i-1, j-1) + 0); // avançem: té cost nul
		if (i-1 < n1-1 and s[(i-1)+1] == t[j-1]) minim = min(minim, f(i, j-1) + 2); // dupliquem una lletra de s, cost 2 
		if (j-1 < n2-1 and s[i-1] == t[(j-1)+1]) minim = min(minim, f(i-1, j) + 2);  // dupliquem una lletra de t, cost 2 
		minim = min(minim, f(i-1, j) + 3); // borrem lletra de s
		minim = min(minim, f(i, j-1) + 3); // borrem lletra de t
	}
	return res = minim;
}


int main() {
	while (cin >> s >> t) {
		n1 = s.size();
		n2 = t.size();
		if (n2 > n1) {  // s serà sempre la paraula més gran
			swap(s, t);
			swap(n1, n2);
		}
		C = VVI(n1+1, VI(n1+1, -1)); // com a molt la paraula nova serà de long n1 (com s)
		cout << f(n1, n2) << endl;
	}
	return 0;
}
