#include <iostream>
#include <vector>

using namespace std;
using VC = vector<char>;
using VVC = vector<VC>;
using VB = vector<bool>;
using VVB = vector<VB>;


int n, m;
VVC T;  // Tauler 
VVB fix;  //fix[i][j] diu si el posicio i,j es fixa

void escriu() {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) cout << T[i][j];
		cout << endl;
	}
	cout << "----------" << endl; 
	return;
}


// Diu si en la posicio i,j s'esta produint tres en ratlla cap amunt o esquerra.
bool tres_en_ratlla (int i, int j, char c) {
	bool horizont = false;
	if (i >= 2) horizont = T[i-2][j] == c and T[i-1][j] == c;
	bool vert = false;
	if (j >= 2) vert = T[i][j-2] == c and T[i][j-1] == c;
	return horizont or vert;
}


// Calcula tots els taulers possibles, donat que anem per i,j.
void f(int i, int j) {
	if (j == m) j = 0, i++;
	if (i == n) {  // Tauler complet
		escriu();
		return;
	}
	else if (i < n and j < m) {
		if (not fix[i][j]) {
			T[i][j] = 'O';
			if (not tres_en_ratlla(i, j, 'O')) f(i, j+1);
			T[i][j] = 'X';
			if (not tres_en_ratlla(i, j, 'X')) f(i, j+1);
		}
		else {
			if (not tres_en_ratlla(i, j, T[i][j])) f(i, j+1);
		}
	}
	return;
}



int main() {
	while (cin >> n >> m) {
		T = VVC(n, VC(m));
		fix = VVB(n, VB(m, false));
		// Llegim tauler
		char c;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cin >> c;
				if (c != '.') {
					T[i][j] = c;
					fix[i][j] = true;
				}
			}
		}
		/*
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) cout << fix[i][j];
			cout << endl;
		}*/
		f(0,0);
		cout << "********************" << endl;
	}
	return 0;
}

